<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\log;
use App\produk;
use Auth;
use DataTables;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Hash;
class MasterController extends Controller
{
    public function master()
    {   
        $tahun="2019";
        $data=produk::with(['detail'=>function($query) use($tahun)
        {
            return $query->where('tahun','=', $tahun);
        }])->get();
        // dd($data->toArray());
        return view('master.beranda',compact('data'));
    }

    public function data_master()
{
    return view('akun.data_master');
}
public function json_master()
{
    $data=User::where('role_id','=',1)->get();
    //dd($data->toArray());
    return Datatables::of($data)
                ->addIndexColumn()
                ->addColumn('action', function ($user) {
                    return '
                    <button type="button" data-toggle="modal" data-target="#update'.$user->id.'" class="btn btn-primary"><i class="fa fa-edit" aria-hidden="true"></i> Update</button>
                    <!-- Modal-->
                    <div id="update'.$user->id.'" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" class="modal fade text-left">
                        <div role="document" class="modal-dialog">
                            <div class="modal-content">
                            
                            <div class="modal-body">
                            <form class="form-horizontal" action="'.route('master.update_master',$user->id).'" method="post" enctype="multipart/form-data">
                            <input type="hidden" name="_token" value="'.csrf_token().'">
                             <div class="row">
                                 <div class="col-md-6">
                                     <label><b>Nama Lengkap*</b></label>
                                     <fieldset class="form-group position-relative has-icon-left">
                                         <input type="text" class="form-control" name="name" placeholder="Nama Lengkap" value="'.$user->name.'">
                                         <div class="form-control-position">
                                         <i class="fa fa-user-o" aria-hidden="true"></i>
                                         </div>
                                     </fieldset>
                                 </div>
                                 <div class="col-md-6">
                                     <label><b>Username*</b></label>
                                     <fieldset class="form-group position-relative has-icon-left">
                                         <input type="text" class="form-control" name="username" placeholder="Username" required value="'.$user->username.'">
                                         <div class="form-control-position">
                                         <i class="fa fa-address-book-o" aria-hidden="true"></i>
                                         </div>
                                     </fieldset>
                                 </div>
                                 <div class="col-md-6">
                                     <label><b>WhatsApp*</b></label>
                                     <fieldset class="form-group position-relative has-icon-left">
                                         <input type="number" class="form-control" name="hp" placeholder="WhatsApp" required value="'.$user->hp.'">
                                         <div class="form-control-position">
                                         <i class="fa fa-whatsapp" aria-hidden="true"></i>
                                         </div>
                                     </fieldset>
                                 </div>
                                 <div class="col-md-6">
                                     <label><b>Foto Profil*</b></label>
                                     <fieldset class="form-group position-relative has-icon-left">
                                         <input type="file" class="form-control" name="pic">
                                         <div class="form-control-position">
                                         <i class="fa fa-picture-o" aria-hidden="true"></i>
                                         </div>
                                     </fieldset>
                                 </div>
                                 </div>
                             <div class="row">
                                <div class="col-md-12 pt-2">
                                    <fieldset class="form-group position-relative has-icon-left">
                                        <button type="submit" class="btn btn-outline-primary btn-block"><i class="far fa-check-square"></i> Verifikasi</button>
                                    </fieldset>
                                </div>
                            </div>
                        </form>
                            </div>
                            <div class="modal-footer">
                                <button type="button" data-dismiss="modal" class="btn btn-xs btn-info">Batal</button>
                            </div>
                            </div>
                        </div>
                        </div>
                        <!-- end of modal -->
                <button type="button" data-toggle="modal" data-target="#hapus'.$user->id.'" class="btn btn-warning"><i class="fa fa-trash" aria-hidden="true"></i> Hapus</button>
                <!-- Modal-->
                <div id="hapus'.$user->id.'" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" class="modal fade text-left">
                    <div role="document" class="modal-dialog">
                        <div class="modal-content">
                        
                        <div class="modal-body">
                            <h4>
                            Yakin Akan Mengapus <b>'.$user->nama_lengkap.'</b>?
                            </h4>
                        </div>
                        <div class="modal-footer">
                            <button type="button" data-dismiss="modal" class="btn btn-xs btn-info">Batal</button>
                            <a href="'.route('master.delete_master',$user->id).'" data-id="'.$user->id.'" class="btn btn-xs btn-danger"><i class="fa fa-trash" aria-hidden="true"></i> Hapus</a>
                            
                        </div>
                        </div>
                    </div>
                    </div>
                    <!-- end of modal -->
                    ';
                })
                ->addColumn('pic', function ($user) {
                    $pic=url('/').Storage::url($user->pic);
                    return'
                    <img src="'.$pic.'" alt="" width="100">
                    ';
                })
                ->escapeColumns([])
                ->make(true);
}

public function input_master(Request $request)
{
    $request->validate([
        'name'=>['required'],
        'username'=>['required'],
        'password'=>['required'],
        'pic'=>'belum',
        'jk'=>['required'],
        'hp'=>['required'],
        ]);
        $password=$request->get('password');
        $user_id = User::insertGetId([
            'name' => $request->get('name'),
            'username' => $request->get('username'), 
            'jk' => $request->get('jk'), 
            'hp' => $request->get('hp'), 
            'password' => Hash::make($password),
            'role_id' => 1, 
            'pic' => "belum", 
    ]);
    return redirect()->back()->with('success', 'Akun Berhasil Dibuat');
}
public function update_master(Request $request, $id)
    {
        $foto = $request->file('pic');
        if ($foto==null) {
            $data=User::find($id);
            $data->name=$request->get('name');
            $data->username=$request->get('username');
            $data->hp=$request->get('hp');
            $pic=$data->pic;
            $data->save();
            return redirect()->back()->with('success', 'Akun Berhasil Update');
        }else{
            $path = $foto->store('public/profil');
            $data=User::find($id);
            $data->name=$request->get('name');
            $data->username=$request->get('username');
            $data->hp=$request->get('hp');
            $data->pic=$path;
            $data->save();
            return redirect()->back()->with('success', 'Akun Berhasil Update');
        }
    }
public function delete_master($id)
{
    $data=User::find($id);
        Storage::delete($data->pic);
        $data->delete();
        return redirect()->back()->with('success', 'Berhasil dihapus');
    }
    public function api()
    {
        $data=User::all();
        return response()->json($data);
    }
}
