<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Produk</title>
    <style>
        table {
            border-collapse: collapse;
        }
        td {
            text-align: center;
        }
        table {
            width: 100%;
        }

        th {
            width: 70px;
        }
    </style>
</head>
<body>
@foreach($produk as $produks)
    <table border="1">
        <thead>
            <th>Produk</th>
            <th>Tahun</th>
            <th>Jan</th>
            <th>Feb</th>
            <th>Mar</th>
            <th>Apr</th>
            <th>Mei</th>
            <th>Jun</th>
            <th>Jul</th>
            <th>Agu</th>
            <th>Sep</th>
            <th>Okt</th>
            <th>Nov</th>
            <th>Des</th>
        </thead>
        <tbody>
            <tr>
                <td rowspan="5">{{$produks->produk_id}}</td>
            </tr>
                    @foreach($tahun as $tahuns)
                        @if($tahuns->produk_id==$produks->produk_id)
                            <tr>
                                <td>{{$tahuns->tahun}}</td>
                                @foreach($data as $datas)
                                    @if($datas->tahun==$tahuns->tahun&&$datas->produk_id==$produks->produk_id)
                                        <td>{{$datas->kuantitas}}</td>
                                    @endif
                                @endforeach
                            </tr>
                        @endif
                    @endforeach
        </tbody>
    </table>
@endforeach
</body>
</html>