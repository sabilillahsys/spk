<!-- BEGIN: Main Menu-->
<div class="main-menu menu-fixed menu-light menu-accordion menu-shadow" data-scroll-to-active="true">
        <div class="main-menu-content">
        <ul class="navigation navigation-main" id="main-menu-navigation" data-menu="menu-navigation">
                <li class=" navigation-header"><span>MENU</span><i class=" feather icon-minus" data-toggle="tooltip" data-placement="right" data-original-title="General"></i>
                </li>
                <li class="nav-item {{ (request()->routeIs('master.master')) ? 'active' : '' }}"><a href="{{route('master.master')}}"><i class="feather icon-home"></i><span class="menu-title" data-i18n="Dashboard"> Dashboard</span></a>
                </li>
                <li class=" nav-item"><a href="#"><i class="feather icon-user"></i><span class="menu-title" data-i18n="Dashboard">Akun</span></a>
                    <ul class="menu-content">
                    <li class="{{ (request()->routeIs('master.data_master')) ? 'active' : '' }}"><a class="menu-item" href="{{route('master.data_master')}}" data-i18n="Master">Admin</a>
                    </li>
                    <!-- <li class="{{ (request()->routeIs('pelanggan.data_pelanggan')) ? 'active' : '' }}"><a class="menu-item" href="{{route('pelanggan.data_pelanggan')}}" data-i18n="Master">Pelanggan</a>
                    </li> -->
                </ul>
                <li class=" nav-item"><a href="#"><i class="fas fa-box"></i><span class="menu-title" data-i18n="Dashboard">Produk</span></a>
                    <ul class="menu-content">
                    <li class="{{ (request()->routeIs('produk.data_produk')) ? 'active' : '' }}"><a class="menu-item" href="{{route('produk.data_produk')}}" data-i18n="Master">Data Produk</a>
                    </li>
                </ul>
                <!-- <li class=" nav-item"><a href="#"><i class="fas fa-book"></i><span class="menu-title" data-i18n="Dashboard">Data</span></a>
                    <ul class="menu-content">
                    <li class="{{ (request()->routeIs('uji.data_uji')) ? 'active' : '' }}"><a class="menu-item" href="{{route('uji.data_uji')}}" data-i18n="Master">Data Uji</a>
                    </li>
                    <li class="{{ (request()->routeIs('latih.data_latih')) ? 'active' : '' }}"><a class="menu-item" href="{{route('latih.data_latih')}}" data-i18n="Master">Data Latih</a>
                    </li>
                </ul> -->
            </ul>
        </div>
    </div>
    <!-- END: Main Menu-->