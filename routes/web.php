<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('home');
});
Route::get('login', function () {
    return view('login');
});
include('master.php');
include('produk.php');
include('data.php');
include('pelanggan.php');
include('import.php');
include('cetak.php');
include('layer.php');
Route::post('masuk', 'LoginController@masuk')->name('masuk');
Route::get('keluar', 'LoginController@keluar')->name('keluar');

