<?php
Route::group(['middleware' => ['web','roles']],function(){
    Route::group(['roles'=>['Master']],function(){
        Route::group([
            'prefix' => 'master','as' => 'master.',
        ], function(){
            Route::get('/','MasterController@master')->name('master');
            Route::get('/api','MasterController@api')->name('api');
            //akun master
            Route::get('/data_master','MasterController@data_master')->name('data_master');
            Route::get('/json_master','MasterController@json_master')->name('json_master');
            Route::post('/input_master','MasterController@input_master')->name('input_master');
            Route::post('/update_master/{id}','MasterController@update_master')->name('update_master');
            Route::get('/delete_master/{id}','MasterController@delete_master')->name('delete_master');
            
        });
    });
});