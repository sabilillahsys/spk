@extends('layouts.tools.layout')

@section('content')
<!-- BEGIN: Content-->
    <div class="app-content content">
        <div class="content-overlay"></div>
        <div class="content-wrapper">
            <div class="content-body">
                <!-- Enable - disable FixedHeader table -->
                <section id="fixedheader">
                    <div class="row">
                        <div class="col-12">
                            <div class="card">
                                <div class="card-header">
                                    <h4 class="card-title">Import Penjualan Produk</h4>
                                    <a class="heading-elements-toggle"><i class="fa fa-ellipsis-v font-medium-3"></i></a>
                                    <div class="heading-elements">
                                        <ul class="list-inline mb-0">
                                            <li><a data-action="collapse"><i class="feather icon-minus"></i></a></li>
                                            <li><a data-action="reload"><i class="feather icon-rotate-cw"></i></a></li>
                                            <li><a data-action="expand"><i class="feather icon-maximize"></i></a></li>
                                            <li><a data-action="close"><i class="feather icon-x"></i></a></li>
                                        </ul>
                                    </div>
                                    <div class="col col-md-12">
                                        <form action="{{route('import.import_penjualan',$id)}}" method="post" enctype="multipart/form-data">
                                                @csrf
                                                <div class="row">
                                                    <div class="col col-md-4">
                                                        <div class="form-group">
                                                            <label>file *</label>
                                                            <input type="file" name="file" class="form-control" required>
                                                        </div>
                                                    </div>
                                                    <div class="col col-md-4">
                                                        <div class="form-group">       
                                                            <label>Import</label>
                                                            <input type="submit" value="Import" class="btn btn-warning btn-block">
                                                        </div>
                                                    </div>
                                                </div>
                                            </form> 
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </section>
                                        <!-- Enable - disable FixedHeader table -->
                <section id="fixedheader">
                    <div class="row">
                        <div class="col-12">
                            <div class="card">
                                <div class="card-header">
                                    <h4 class="card-title">Set Penjualan Produk</h4>
                                    <a class="heading-elements-toggle"><i class="fa fa-ellipsis-v font-medium-3"></i></a>
                                    <div class="heading-elements">
                                        <ul class="list-inline mb-0">
                                            <li><a data-action="collapse"><i class="feather icon-minus"></i></a></li>
                                            <li><a data-action="reload"><i class="feather icon-rotate-cw"></i></a></li>
                                            <li><a data-action="expand"><i class="feather icon-maximize"></i></a></li>
                                            <li><a data-action="close"><i class="feather icon-x"></i></a></li>
                                        </ul>
                                    </div>
                                    <br>
                                    <div class="col col-md-12">
                                    <div class="row">
                                        <div class="col col-md-2">
                                            <button type="button" data-toggle="modal" data-target="#add_data" class="btn btn-block btn-info">Tambah Data</button>
                                             <!-- Modal-->
                                            <div id="add_data" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" class="modal fade text-left">
                                                <div role="document" class="modal-dialog">
                                                    <div class="modal-content">
                                                        <div class="modal-body">
                                                            <form action="{{route('produk.input_penjualan_produk')}}" method="post">
                                                                @csrf
                                                                <div class="row">
                                                                    <div class="col col-md-6">
                                                                        <input type="hidden" name="produk_id" class="form-control" value="{{$id}}">
                                                                        <div class="form-group">
                                                                            <label>Kuantitas *</label>
                                                                            <input type="text" name="kuantitas" class="form-control" placeholder="Kuantitas" required>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-md-6">
                                                                        <label><b>Bulan *</b></label>
                                                                        <fieldset class="form-group position-relative has-icon-left">
                                                                            <select class="form-control" name="bulan"  required>
                                                                                <option value="01" >Januari</option>
                                                                                <option value="02" >Februari</option>
                                                                                <option value="03" >Maret</option>
                                                                                <option value="04" >April</option>
                                                                                <option value="05" >Mei</option>
                                                                                <option value="06" >Juni</option>
                                                                                <option value="07" >Juli</option>
                                                                                <option value="08" >Agustus</option>
                                                                                <option value="09" >September</option>
                                                                                <option value="10" >Oktober</option>
                                                                                <option value="11" >November</option>
                                                                                <option value="12" >Desember</option>
                                                                            </select>
                                                                            <div class="form-control-position">
                                                                            </div>
                                                                            </fieldset>
                                                                        </div>
                                                                        <div class="col col-md-6">
                                                                        <div class="form-group">
                                                                            <label>Tahun *</label>
                                                                            <input type="number" name="tahun" class="form-control" placeholder="Tahun" required>
                                                                        </div>
                                                                        </div>
                                                                        <div class="col col-md-6">
                                                                        <div class="form-group">
                                                                            <label>Total Harga *</label>
                                                                            <input type="number" name="total_harga" class="form-control" placeholder="Total Harga" required>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col col-md-12">
                                                                        <div class="form-group">       
                                                                            <input type="submit" value="Tambah" class="btn btn-block btn-primary">
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </form> 
                                                        </div>
                                                        <div class="modal-footer">
                                                            <button type="button" data-dismiss="modal" class="btn btn-xs btn-info">Batal</button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- end of modal -->
                                        </div>
                                        <div class="col col-md-2">
                                            <a href="{{route('cetak.cetak_produk',$id)}}" class="btn btn-warning btn-block">Data Produk</a>
                                        </div>
                                        <div class="col col-md-2">
                                            <a href="{{route('cetak.created_dataset',$id)}}" class="btn btn-danger btn-block">Set Dataset</a>
                                        </div>
                                        <div class="col col-md-2">
                                            <button type="button" data-toggle="modal" data-target="#training_step" class="btn btn-block btn-warning">Training Step</button>
                                             <!-- Modal-->
                                            <div id="training_step" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" class="modal fade text-left">
                                                <div role="document" class="modal-dialog">
                                                    <div class="modal-content">
                                                        <div class="modal-body">
                                                            <form action="{{route('cetak.training_step',$id)}}" method="get">
                                                                @csrf
                                                                <div class="row">
                                                                    <div class="col col-md-12">
                                                                        <div class="form-group">
                                                                            <label>Persentase *</label>
                                                                            <input type="number" name="persen" class="form-control" placeholder="Persen Training" required>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col col-md-12">
                                                                        <div class="form-group">
                                                                            <label>Jumlah Perulangan *</label>
                                                                            <input type="number" name="batas" class="form-control" placeholder="Jumlah Perulangan" required>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col col-md-12">
                                                                        <div class="form-group">       
                                                                            <input type="submit" value="Training" class="btn btn-block btn-primary">
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </form> 
                                                        </div>
                                                        <div class="modal-footer">
                                                            <button type="button" data-dismiss="modal" class="btn btn-xs btn-info">Batal</button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- end of modal -->
                                        </div>
                                        <div class="col col-md-2">
                                            <button type="button" data-toggle="modal" data-target="#training" class="btn btn-block btn-warning">Training</button>
                                             <!-- Modal-->
                                            <div id="training" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" class="modal fade text-left">
                                                <div role="document" class="modal-dialog">
                                                    <div class="modal-content">
                                                        <div class="modal-body">
                                                            <form action="{{route('cetak.training',$id)}}" method="get">
                                                                @csrf
                                                                <div class="row">
                                                                    <div class="col col-md-12">
                                                                        <div class="form-group">
                                                                            <label>Persentase *</label>
                                                                            <input type="number" name="persen" class="form-control" placeholder="Persen Training" required>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col col-md-12">
                                                                        <div class="form-group">
                                                                            <label>Jumlah Perulangan *</label>
                                                                            <input type="number" name="batas" class="form-control" placeholder="Jumlah Perulangan" required>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col col-md-12">
                                                                        <div class="form-group">       
                                                                            <input type="submit" value="Training" class="btn btn-block btn-primary">
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </form> 
                                                        </div>
                                                        <div class="modal-footer">
                                                            <button type="button" data-dismiss="modal" class="btn btn-xs btn-info">Batal</button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- end of modal -->
                                        </div>
                                        <div class="col col-md-1">
                                        <button type="button" data-toggle="modal" data-target="#testing" class="btn btn-block btn-success">Testing</button>
                                             <!-- Modal-->
                                            <div id="testing" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" class="modal fade text-left">
                                                <div role="document" class="modal-dialog">
                                                    <div class="modal-content">
                                                        <div class="modal-body">
                                                            <form action="{{route('cetak.testing_step',$id)}}" method="get">
                                                                @csrf
                                                                <div class="row">
                                                                    <div class="col col-md-12">
                                                                        <div class="form-group">
                                                                            <label>Persentase *</label>
                                                                            <input type="number" name="persen" class="form-control" placeholder="Persen testing" required>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col col-md-12">
                                                                        <div class="form-group">
                                                                            <label>Jumlah Prediksi *</label>
                                                                            <input type="number" name="jum" class="form-control" placeholder="Jumlah Prediksi" required>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col col-md-12">
                                                                        <div class="form-group">
                                                                            <label>Jumlah Perulangan *</label>
                                                                            <input type="number" name="batas" class="form-control" placeholder="Jumlah Perulangan" required>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col col-md-12">
                                                                        <div class="form-group">       
                                                                            <input type="submit" value="Training" class="btn btn-block btn-primary">
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </form> 
                                                        </div>
                                                        <div class="modal-footer">
                                                            <button type="button" data-dismiss="modal" class="btn btn-xs btn-info">Batal</button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- end of modal -->
                                        </div>
                                        <div class="col col-md-1">
                                        <button type="button" data-toggle="modal" data-target="#prediksi" class="btn btn-block btn-success">Prediksi</button>
                                             <!-- Modal-->
                                            <div id="prediksi" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" class="modal fade text-left">
                                                <div role="document" class="modal-dialog">
                                                    <div class="modal-content">
                                                        <div class="modal-body">
                                                            <form action="{{route('cetak.testing',$id)}}" method="get">
                                                                @csrf
                                                                <div class="row">
                                                                    <div class="col col-md-12">
                                                                        <div class="form-group">
                                                                            <label>Persentase Testing*</label>
                                                                            <input type="number" name="persen" class="form-control" placeholder="Persen testing" required>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col col-md-12">
                                                                        <div class="form-group">
                                                                            <label>Jumlah Prediksi *</label>
                                                                            <input type="number" name="jum" class="form-control" placeholder="Jumlah Prediksi" required>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col col-md-12">
                                                                        <div class="form-group">
                                                                            <label>Jumlah Perulangan *</label>
                                                                            <input type="number" name="batas" class="form-control" placeholder="Jumlah Perulangan" required>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col col-md-12">
                                                                        <div class="form-group">       
                                                                            <input type="submit" value="Training" class="btn btn-block btn-primary">
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </form> 
                                                        </div>
                                                        <div class="modal-footer">
                                                            <button type="button" data-dismiss="modal" class="btn btn-xs btn-info">Batal</button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- end of modal -->
                                        </div>
                                    </div>
                                </div>
                                </div>
                                </div>
                            </div>
                        </section>
                <!-- Enable - disable FixedHeader table -->
                <section id="fixedheader">
                    <div class="row">
                        <div class="col-12">
                            <div class="card">
                                <div class="card-header">
                                    <h4 class="card-title">Data Penjualan Produk</h4>
                                    <a class="heading-elements-toggle"><i class="fa fa-ellipsis-v font-medium-3"></i></a>
                                    <div class="heading-elements">
                                        <ul class="list-inline mb-0">
                                            <li><a data-action="collapse"><i class="feather icon-minus"></i></a></li>
                                            <li><a data-action="reload"><i class="feather icon-rotate-cw"></i></a></li>
                                            <li><a data-action="expand"><i class="feather icon-maximize"></i></a></li>
                                            <li><a data-action="close"><i class="feather icon-x"></i></a></li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="card-content collapse show">
                                <div class="card-body card-dashboard">
                                    <div class="row" style="padding-bottom:10px;">
                                        <div class="col-md-12">
                                            
                                        </div>
                                            <div class="container table-responsive">                        
                                                <table class="table table-striped table-bordered" id="data-pelanggan">
                                                    <thead>
                                                        <tr>
                                                            <th></th>
                                                            <th>No</th>
                                                            <th>Nama Produk</th>
                                                            <th>Kuantitas</th>
                                                            <th>Bulan</th>
                                                            <th>Tahun</th>
                                                            <th>Harga Total</th>
                                                        </tr>
                                                    </thead>
                                                </table>
                                            </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
                <!--/ Enable - disable FixedHeader table -->
            </div>
        </div>
    </div>
<!-- END: Content-->
@endsection
@push('scripts')
<script>
//fucktion awal
function format ( d ) {
    // `d` is the original data object for the row
    return '<table cellpadding="5" cellspacing="0" border="0" style="padding-left:50px;">'+
    
    '<tr>'+
            '<td>Aksi:</td>'+
            '<td>'+d.action+'</td>'+
        '</tr>'+
    '</table>';
}
//end function
$(document).ready(function() {
    var table = $('#data-pelanggan').DataTable({
        processing: true,
        serverSide: true,
        scrollCollapse: true,
        ajax: "{{route('produk.json_penjualan_produk',$id)}}",
        columns: [
            {"className": 'details-control',"orderable": false,"data": null,"defaultContent": ''},
            { data: 'DT_RowIndex', name: 'DT_RowIndex'},
            { data: 'produk', name: 'produk' },
            { data: 'kuantitas', name: 'kuantitas' },
            { data: 'bulan', name: 'bulan' },
            { data: 'tahun', name: 'tahun' },
            { data: 'total_harga', name: 'total_harga' },
        ],
    });
// Add event listener for opening and closing details
    $('#data-pelanggan tbody').on('click', 'td.details-control', function () {
        var tr = $(this).closest('tr');
        var row = table.row( tr );
 
        if ( row.child.isShown() ) {
            // This row is already open - close it
            row.child.hide();
            tr.removeClass('shown');
        }
        else {
            // Open this row
            row.child( format(row.data()) ).show();
            tr.addClass('shown');
        }
    });
// Batas bawah
});
</script>
@endpush