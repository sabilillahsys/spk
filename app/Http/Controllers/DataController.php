<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\data_uji;
use App\data_latih;
use App\log;
use Auth;
use DataTables;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Hash;
class DataController extends Controller
{
    public function data_uji()
{
    return view('data.data_uji');
}
public function json_uji()
{
    $data=data_uji::all();
    //dd($data->toArray());
    return Datatables::of($data)
                ->addIndexColumn()
                ->addColumn('action', function ($user) {
                    return '
                    <button type="button" data-toggle="modal" data-target="#update'.$user->id.'" class="btn btn-primary"><i class="fa fa-edit" aria-hidden="true"></i> Update</button>
                    <!-- Modal-->
                    <div id="update'.$user->id.'" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" class="modal fade text-left">
                        <div role="document" class="modal-dialog">
                            <div class="modal-content">
                            
                            <div class="modal-body">
                            <form class="form-horizontal" action="'.route('uji.update_uji',$user->id).'" method="post" enctype="multipart/form-data">
                            <input type="hidden" name="_token" value="'.csrf_token().'">
                             <div class="row">
                                 <div class="col-md-6">
                                     <label><b>Tahun*</b></label>
                                     <fieldset class="form-group position-relative has-icon-left">
                                         <input type="number" class="form-control" name="tahun" placeholder="Nama Produk" value="'.$user->tahun.'">
                                         <div class="form-control-position">
                                         <i class="fa fa-user-o" aria-hidden="true"></i>
                                         </div>
                                     </fieldset>
                                 </div>
                                 <div class="col-md-6">
                                     <label><b>Bulan*</b></label>
                                     <fieldset class="form-group position-relative has-icon-left">
                                     <select class="form-control" name="bulan"  required>
                                     <option value="Januari" >Januari</option>
                                     <option value="Februari" >Februari</option>
                                     <option value="Maret" >Maret</option>
                                     <option value="April" >April</option>
                                     <option value="Mei" >Mei</option>
                                     <option value="Juni" >Juni</option>
                                     <option value="Juli" >Juli</option>
                                     <option value="Agustus" >Agustus</option>
                                     <option value="September" >September</option>
                                     <option value="Oktober" >Oktober</option>
                                     <option value="November" >November</option>
                                     <option value="Desember" >Desember</option>
                                     </select>
                                     </fieldset>
                                 </div>
                                 <div class="col-md-6">
                                     <label><b>Jumlah Penjualan*</b></label>
                                     <fieldset class="form-group position-relative has-icon-left">
                                         <input type="number" class="form-control" name="jumlah_penjualan" placeholder="Jumlah Penjualan" required value="'.$user->jumlah_penjualan.'">
                                         <div class="form-control-position">
                                         <i class="fa fa-whatsapp" aria-hidden="true"></i>
                                         </div>
                                     </fieldset>
                                 </div>
                                 </div>
                             <div class="row">
                                <div class="col-md-12 pt-2">
                                    <fieldset class="form-group position-relative has-icon-left">
                                        <button type="submit" class="btn btn-outline-primary btn-block"><i class="far fa-check-square"></i> Verifikasi</button>
                                    </fieldset>
                                </div>
                            </div>
                        </form>
                            </div>
                            <div class="modal-footer">
                                <button type="button" data-dismiss="modal" class="btn btn-xs btn-info">Batal</button>
                            </div>
                            </div>
                        </div>
                        </div>
                        <!-- end of modal -->
                <button type="button" data-toggle="modal" data-target="#hapus'.$user->id.'" class="btn btn-warning"><i class="fa fa-trash" aria-hidden="true"></i> Hapus</button>
                <!-- Modal-->
                <div id="hapus'.$user->id.'" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" class="modal fade text-left">
                    <div role="document" class="modal-dialog">
                        <div class="modal-content">
                        
                        <div class="modal-body">
                            <h4>
                            Yakin Akan Mengapus Produk <b>'.$user->nama.'</b>?
                            </h4>
                        </div>
                        <div class="modal-footer">
                            <button type="button" data-dismiss="modal" class="btn btn-xs btn-info">Batal</button>
                            <a href="'.route('uji.delete_uji',$user->id).'" data-id="'.$user->id.'" class="btn btn-xs btn-danger"><i class="fa fa-trash" aria-hidden="true"></i> Hapus</a>
                            
                        </div>
                        </div>
                    </div>
                    </div>
                    <!-- end of modal -->
                    ';
                })
                ->escapeColumns([])
                ->make(true);
}

public function input_uji(Request $request)
{
    $request->validate([
        'tahun'=>['required'],
        'bulan'=>['required'],
        'jumlah_penjualan'=>['required'],
        ]);
            data_uji::create([
            'tahun' => $request->get('tahun'),
            'bulan' => $request->get('bulan'), 
            'jumlah_penjualan' => $request->get('jumlah_penjualan'), 
    ]);
    return redirect()->back()->with('success', 'Berhasil Dibuat');
}
public function update_uji(Request $request, $id)
    {
            $data=data_uji::find($id);
            $data->tahun=$request->get('tahun');
            $data->bulan=$request->get('bulan');
            $data->jumlah_penjualan=$request->get('jumlah_penjualan');
            $data->save();
            return redirect()->back()->with('success', 'Berhasil Update');
    }
public function delete_uji($id)
{
    $data=data_uji::find($id);
    $data->delete();
    return redirect()->back()->with('success', 'Berhasil dihapus');
    }
    public function api()
    {
        $data=data_uji::all();
        return response()->json($data);
    }
    public function data_latih()
{
    return view('data.data_latih');
}
public function json_latih()
{
    $data=data_latih::all();
    //dd($data->toArray());
    return Datatables::of($data)
                ->addIndexColumn()
                ->addColumn('action', function ($user) {
                    return '
                    <button type="button" data-toggle="modal" data-target="#update'.$user->id.'" class="btn btn-primary"><i class="fa fa-edit" aria-hidden="true"></i> Update</button>
                    <!-- Modal-->
                    <div id="update'.$user->id.'" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" class="modal fade text-left">
                        <div role="document" class="modal-dialog">
                            <div class="modal-content">
                            
                            <div class="modal-body">
                            <form class="form-horizontal" action="'.route('latih.update_latih',$user->id).'" method="post" enctype="multipart/form-data">
                            <input type="hidden" name="_token" value="'.csrf_token().'">
                             <div class="row">
                                 <div class="col-md-6">
                                     <label><b>Tahun*</b></label>
                                     <fieldset class="form-group position-relative has-icon-left">
                                         <input type="number" class="form-control" name="tahun" placeholder="Nama Produk" value="'.$user->tahun.'">
                                         <div class="form-control-position">
                                         <i class="fa fa-user-o" aria-hidden="true"></i>
                                         </div>
                                     </fieldset>
                                 </div>
                                 <div class="col-md-6">
                                     <label><b>Bulan*</b></label>
                                     <fieldset class="form-group position-relative has-icon-left">
                                     <select class="form-control" name="bulan"  required>
                                     <option value="Januari" >Januari</option>
                                     <option value="Februari" >Februari</option>
                                     <option value="Maret" >Maret</option>
                                     <option value="April" >April</option>
                                     <option value="Mei" >Mei</option>
                                     <option value="Juni" >Juni</option>
                                     <option value="Juli" >Juli</option>
                                     <option value="Agustus" >Agustus</option>
                                     <option value="September" >September</option>
                                     <option value="Oktober" >Oktober</option>
                                     <option value="November" >November</option>
                                     <option value="Desember" >Desember</option>
                                     </select>
                                     </fieldset>
                                 </div>
                                 <div class="col-md-6">
                                     <label><b>Jumlah Penjualan*</b></label>
                                     <fieldset class="form-group position-relative has-icon-left">
                                         <input type="number" class="form-control" name="jumlah_penjualan" placeholder="Jumlah Penjualan" required value="'.$user->jumlah_penjualan.'">
                                         <div class="form-control-position">
                                         <i class="fa fa-whatsapp" aria-hidden="true"></i>
                                         </div>
                                     </fieldset>
                                 </div>
                                 </div>
                             <div class="row">
                                <div class="col-md-12 pt-2">
                                    <fieldset class="form-group position-relative has-icon-left">
                                        <button type="submit" class="btn btn-outline-primary btn-block"><i class="far fa-check-square"></i> Verifikasi</button>
                                    </fieldset>
                                </div>
                            </div>
                        </form>
                            </div>
                            <div class="modal-footer">
                                <button type="button" data-dismiss="modal" class="btn btn-xs btn-info">Batal</button>
                            </div>
                            </div>
                        </div>
                        </div>
                        <!-- end of modal -->
                <button type="button" data-toggle="modal" data-target="#hapus'.$user->id.'" class="btn btn-warning"><i class="fa fa-trash" aria-hidden="true"></i> Hapus</button>
                <!-- Modal-->
                <div id="hapus'.$user->id.'" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" class="modal fade text-left">
                    <div role="document" class="modal-dialog">
                        <div class="modal-content">
                        
                        <div class="modal-body">
                            <h4>
                            Yakin Akan Mengapus Produk <b>'.$user->nama.'</b>?
                            </h4>
                        </div>
                        <div class="modal-footer">
                            <button type="button" data-dismiss="modal" class="btn btn-xs btn-info">Batal</button>
                            <a href="'.route('latih.delete_latih',$user->id).'" data-id="'.$user->id.'" class="btn btn-xs btn-danger"><i class="fa fa-trash" aria-hidden="true"></i> Hapus</a>
                            
                        </div>
                        </div>
                    </div>
                    </div>
                    <!-- end of modal -->
                    ';
                })
                ->escapeColumns([])
                ->make(true);
}

public function input_latih(Request $request)
{
    $request->validate([
        'tahun'=>['required'],
        'bulan'=>['required'],
        'jumlah_penjualan'=>['required'],
        ]);
            data_latih::create([
            'tahun' => $request->get('tahun'),
            'bulan' => $request->get('bulan'), 
            'jumlah_penjualan' => $request->get('jumlah_penjualan'), 
    ]);
    return redirect()->back()->with('success', 'Berhasil Dibuat');
}
public function update_latih(Request $request, $id)
    {
            $data=data_latih::find($id);
            $data->tahun=$request->get('tahun');
            $data->bulan=$request->get('bulan');
            $data->jumlah_penjualan=$request->get('jumlah_penjualan');
            $data->save();
            return redirect()->back()->with('success', 'Berhasil Update');
    }
public function delete_latih($id)
{
    $data=data_latih::find($id);
    $data->delete();
    return redirect()->back()->with('success', 'Berhasil dihapus');
    }
}
