<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDatasetsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('datasets', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('produk_id')->unsigned();
            $table->foreign('produk_id')
                    ->references('id')
                    ->on('produks')
                    ->onDelete('CASCADE')
                    ->onUpdate('cascade');
            $table->text('x1')->nullable();
            $table->text('x2')->nullable();
            $table->text('x3')->nullable();
            $table->text('x4')->nullable();
            $table->text('x5')->nullable();
            $table->text('x6')->nullable();
            $table->text('x7')->nullable();
            $table->text('x8')->nullable();
            $table->text('x9')->nullable();
            $table->text('x10')->nullable();
            $table->text('x11')->nullable();
            $table->text('x12')->nullable();
            $table->text('target')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('datasets');
    }
}
