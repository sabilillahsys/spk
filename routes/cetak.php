<?php
Route::group(['middleware' => ['web','roles']],function(){
    Route::group(['roles'=>['Master','Pelanggan']],function(){
        Route::group([
            'prefix' => 'cetak','as' => 'cetak.',
        ], function(){
            Route::get('/cetak_produk/{id}','CetakController@cetak_produk')->name('cetak_produk');
            Route::get('/cetak_produk_all','CetakController@cetak_produk_all')->name('cetak_produk_all');
            Route::get('/cetak_kom/{id}','CetakController@cetak_kom')->name('cetak_kom');
            Route::get('/created_dataset/{id}','CetakController@created_dataset')->name('created_dataset');
            Route::get('/training/{id}','CetakController@training')->name('training');
            Route::get('/training_step/{id}','CetakController@training_step')->name('training_step');
            Route::get('/testing_step/{id}','CetakController@testing_step')->name('testing_step');
            Route::get('/testing/{id}','CetakController@testing')->name('testing');
        });
    });
});