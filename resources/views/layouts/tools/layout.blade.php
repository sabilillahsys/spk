<!DOCTYPE html>
<html class="loading" lang="en" data-textdirection="ltr">
<!-- BEGIN: Head-->

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    <meta name="description" content="Penerapan Sisitem Peramalan Produk Olahan Salak Menggunakan Metode BACKPROPOGATION NEURAL NETWORK">
    <meta name="keywords" content="Spk, skripsi Siti Nurizzatin Kamala, Neural Network, Backpropagation, Peramalan, Akurasi">
    <meta name="author" content="Siti Nurizzatin Kamala">
    <title>Siti Nurizzatin Kamala</title>
    <link rel="apple-touch-icon" href="{{asset('app-assets/images/ico/apple-icon-120.png')}}">
    <link rel="shortcut icon" type="image/x-icon" href="{{asset('app-assets/images/ico/favicon.ico')}}">
    <link href="https://fonts.googleapis.com/css?family=Montserrat:300,300i,400,400i,500,500i%7COpen+Sans:300,300i,400,400i,600,600i,700,700i" rel="stylesheet">
    @include('layouts.tools.css')
    <style>
      .my-custom-scrollbar {
        position: relative;
        height: 100px;
        overflow: auto;
        margin:3px;
        }
        .table-wrapper-scroll-y {
        display: block;
        }
    </style>
</head>
<!-- END: Head-->

<!-- BEGIN: Body-->

<body class="vertical-layout vertical-menu 2-columns   fixed-navbar" data-open="click" data-menu="vertical-menu" data-col="2-columns">

    <!-- BEGIN: Header-->
   @include('layouts.tools.nav')
    <!-- END: Header-->


    <!-- BEGIN: Main Menu-->
                    @if(Auth::user()->role_id==1)
                    @include('master.tools.side')
                    @endif
                    @if(Auth::user()->role_id==2)
                    @include('pelanggan.tools.side')
                    @endif
    <!-- END: Main Menu-->

    <!-- BEGIN: Content-->
    @yield('content')
    <!-- END: Content-->

    <div class="sidenav-overlay"></div>
    <div class="drag-target"></div>

    <!-- BEGIN: Footer-->
    @include('layouts.tools.footer')
    <!-- END: Footer-->

    @include('layouts.tools.js')
    @include('alert')
    @stack('scripts')
</body>
<!-- END: Body-->

</html>