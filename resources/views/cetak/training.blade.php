<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Produk</title>
    <style>
        table {
            border-collapse: collapse;
        }
        td {
            text-align: center;
        }
        table {
            width: 100%;
        }

        th {
            width: 70px;
        }
    </style>
</head>
<body>
    <table border="1">
        <thead>
            <th>P</th>
            <th>x1</th>
            <th>x2</th>
            <th>x3</th>
            <th>x4</th>
            <th>x5</th>
            <th>x6</th>
            <th>x7</th>
            <th>x8</th>
            <th>x9</th>
            <th>x10</th>
            <th>x11</th>
            <th>x12</th>
            <th>T</th>
        </thead>
        <tbody>
            @php($no=1)
            @foreach($dataset as $datas)
            @php($d_x1[]=$datas->x1)
            @php($d_x2[]=$datas->x2)
            @php($d_x3[]=$datas->x3)
            @php($d_x4[]=$datas->x4)
            @php($d_x5[]=$datas->x5)
            @php($d_x6[]=$datas->x6)
            @php($d_x7[]=$datas->x7)
            @php($d_x8[]=$datas->x8)
            @php($d_x9[]=$datas->x9)
            @php($d_x10[]=$datas->x10)
            @php($d_x11[]=$datas->x11)
            @php($d_x12[]=$datas->x12)
            @php($d_target[]=$datas->target)
                <tr>
                    <td>{{$no}}</td>
                    <td>{{$datas->x1}}</td>
                    <td>{{$datas->x2}}</td>
                    <td>{{$datas->x3}}</td>
                    <td>{{$datas->x4}}</td>
                    <td>{{$datas->x5}}</td>
                    <td>{{$datas->x6}}</td>
                    <td>{{$datas->x7}}</td>
                    <td>{{$datas->x8}}</td>
                    <td>{{$datas->x9}}</td>
                    <td>{{$datas->x10}}</td>
                    <td>{{$datas->x11}}</td>
                    <td>{{$datas->x12}}</td>
                    <td>{{$datas->target}}</td>
                </tr>
            @php($no++)
            @endforeach
            <tr>
                <td colspan="14"></td>
            </tr>
            <tr>
                <td><b>MIN</b></td>
                <td>{{min($d_x1)}}</td>
                <td>{{min($d_x2)}}</td>
                <td>{{min($d_x3)}}</td>
                <td>{{min($d_x4)}}</td>
                <td>{{min($d_x5)}}</td>
                <td>{{min($d_x6)}}</td>
                <td>{{min($d_x7)}}</td>
                <td>{{min($d_x8)}}</td>
                <td>{{min($d_x9)}}</td>
                <td>{{min($d_x10)}}</td>
                <td>{{min($d_x11)}}</td>
                <td>{{min($d_x12)}}</td>
                <td>{{min($d_target)}}</td>
            </tr>
            <tr>
                <td><b>MAX</b></td>
                <td>{{max($d_x1)}}</td>
                <td>{{max($d_x2)}}</td>
                <td>{{max($d_x3)}}</td>
                <td>{{max($d_x4)}}</td>
                <td>{{max($d_x5)}}</td>
                <td>{{max($d_x6)}}</td>
                <td>{{max($d_x7)}}</td>
                <td>{{max($d_x8)}}</td>
                <td>{{max($d_x9)}}</td>
                <td>{{max($d_x10)}}</td>
                <td>{{max($d_x11)}}</td>
                <td>{{max($d_x12)}}</td>
                <td>{{max($d_target)}}</td>
            </tr>
        </tbody>
    </table>
    <br>
    <h3>Normalisasi</h3>
    <table border="1">
        <thead>
            <th>P</th>
            <th>x1</th>
            <th>x2</th>
            <th>x3</th>
            <th>x4</th>
            <th>x5</th>
            <th>x6</th>
            <th>x7</th>
            <th>x8</th>
            <th>x9</th>
            <th>x10</th>
            <th>x11</th>
            <th>x12</th>
            <th>T</th>
        </thead>
        <tbody>
            @php($no=1)
            @foreach($dataset as $datas)
            @php($d_x1[]=$datas->x1)
            @php($d_x2[]=$datas->x2)
            @php($d_x3[]=$datas->x3)
            @php($d_x4[]=$datas->x4)
            @php($d_x5[]=$datas->x5)
            @php($d_x6[]=$datas->x6)
            @php($d_x7[]=$datas->x7)
            @php($d_x8[]=$datas->x8)
            @php($d_x9[]=$datas->x9)
            @php($d_x10[]=$datas->x10)
            @php($d_x11[]=$datas->x11)
            @php($d_x12[]=$datas->x12)
            @php($d_target[]=$datas->target)
                <tr>
                    <td>{{$no}}</td>
                    <td>
                        @php($x1=0.8*($datas->x1-min($d_x1))/(max($d_x1)-min($d_x1))+0.1)
                        {{$x1}}
                        @php($xa[0][]=$x1)
                    </td>
                    <td> @php($x2=0.8*($datas->x2-min($d_x2))/(max($d_x2)-min($d_x2))+0.1)
                        {{$x2}}
                        @php($xa[1][]=$x2)
                    </td>
                    <td> @php($x3=0.8*($datas->x3-min($d_x3))/(max($d_x3)-min($d_x3))+0.1)
                        {{$x3}}
                        @php($xa[2][]=$x3)
                    </td>
                    <td> @php($x4=0.8*($datas->x4-min($d_x4))/(max($d_x4)-min($d_x4))+0.1)
                        {{$x4}}
                        @php($xa[3][]=$x4)
                    </td>
                    <td> @php($x5=0.8*($datas->x5-min($d_x5))/(max($d_x5)-min($d_x5))+0.1)
                        {{$x5}}
                        @php($xa[4][]=$x5)
                    </td>
                    <td> @php($x6=0.8*($datas->x6-min($d_x6))/(max($d_x6)-min($d_x6))+0.1)
                        {{$x6}}
                        @php($xa[5][]=$x6)
                    </td>
                    <td> @php($x7=0.8*($datas->x7-min($d_x7))/(max($d_x7)-min($d_x7))+0.1)
                        {{$x7}}
                        @php($xa[6][]=$x7)
                    </td>
                    <td> @php($x8=0.8*($datas->x8-min($d_x8))/(max($d_x8)-min($d_x8))+0.1)
                        {{$x8}}
                        @php($xa[7][]=$x8)
                    </td>
                    <td> @php($x9=0.8*($datas->x9-min($d_x9))/(max($d_x9)-min($d_x9))+0.1)
                        {{$x9}}
                        @php($xa[8][]=$x9)
                    </td>
                    <td> @php($x10=0.8*($datas->x10-min($d_x10))/(max($d_x10)-min($d_x10))+0.1)
                        {{$x10}}
                        @php($xa[9][]=$x10)
                    </td>
                    <td> @php($x11=0.8*($datas->x11-min($d_x11))/(max($d_x11)-min($d_x11))+0.1)
                        {{$x11}}
                        @php($xa[10][]=$x11)
                    </td>
                    <td> @php($x12=0.8*($datas->x12-min($d_x12))/(max($d_x12)-min($d_x12))+0.1)
                        {{$x12}}
                        @php($xa[11][]=$x12)
                    </td>
                    <td>
                        @php($target=0.8*($datas->target-min($d_target))/(max($d_target)-min($d_target))+0.1)
                        {{$target}}
                        @php($xtarget[]=$target)
                    </td>
                </tr>
            @php($no++)
            @endforeach
            
        </tbody>
    </table>
    <br>
    <h3>Bobot Input Layer</h3>
    <table border="1">
            @foreach($bobot_i as $boi)
                @php($boi_arr[]= $boi->nilai)
            @endforeach
            @php($s=0)
            @for($i=0; $i < 12; $i++)
                <tr>
                @for($j=0; $j < $hl->nilai; $j++)
                    <td>{{$bobot_i_arr[$s]['nilai']}}</td>
                    @php($m_bobot_i[$i][$j]=$boi_arr[$s])
                @php($s++)
                @endfor
                </tr>
            @endfor
    </table>
    <br>
    <h3>Bobot Output Layer</h3>
    <table border="1">
        <tr>
        @foreach($bobot_o as $boo)
            <td>{{$boo->nilai}}</td>
            @php( $boo_arr[]=$boo->nilai)
        @endforeach
        </tr>
    </table>
    <br>
    <h3>Bias Input Layer</h3>
    <table border="1">
        <tr>
        @foreach($bias_i as $bi)
            <td>{{$bi->nilai}}</td>
            @php($bi_arr[]=$bi->nilai)
        @endforeach
        </tr>
    </table>
    <br>
    <h3>Bias Output Layer</h3>
    <table border="1">
        <tr>
            <td>@php($bo_arr[]=$bias_o->nilai)
                {{$bias_o->nilai}}
            </td>
        </tr>
    </table>
    <h3>Learning Rate</h3>
    <table border="1">
        <tr>
            <td>{{$learning_rate->nilai}}</td>
        </tr>
    </table>
    @for($sk=0 ; $sk < $batas ; $sk++)
    @php($not=0)
    @foreach($dataset as $dat)
   
        
        @for($j=0; $j < $hl->nilai; $j++)  
        
            @php($sum=0)
            @for($i=0; $i < 12; $i++)
                @php($sum+=($m_bobot_i[$i][$j]*$xa[$i][$not]))
            @endfor
            @php($z=$bi_arr[$j]+$sum)
            
            @php($z_arr[$not][]=$z)
        
        @endfor
    <!-- LANGKAH 2 -->
            @for($i=0; $i < $hl->nilai; $i++)
            
                @php($fz=1/(1+(pow(2.71828183,-$z_arr[$not][$i]))))
                @php($fz_arr[$not][]=$fz)
            
            @endfor
    <!-- LANGKAH 3 -->
            @php($yn=0)
            @for($k=0;$k<(count($boo_arr));$k++)
                @php($yn+=($fz_arr[$not][$k])*$boo_arr[$k])
            @endfor
            @if($not==0)
            @php($yn=$bo_arr[$not]+$yn)
            @else
            @php($yn=$bo_arr[$not-1]+$yn)
            @endif
            @php($yn_arr[$not]=$yn)
    <!-- LANGKAH 4 -->
            @php($fyn=1/(1+(pow(2.71828183,-$yn_arr[$not]))))
                @php($fyn_arr[$not]=$fyn)
    <!-- LANGKAH 5 -->
            @php($b=($xtarget[$not]-$fyn_arr[$not])*($fyn_arr[$not])*(1-$fyn_arr[$not]))
            @php($b_arr[$not]=$b)
    <!-- LANGKAH 6 -->
            @for($i=0; $i < $hl->nilai; $i++)
                @php($aw=$learning_rate->nilai*$b_arr[$not]*$fz_arr[$not][$i])
                @php($aw_arr[$not][]=$aw)
            @endfor
    <!-- LANGKAH 7 -->
            @php($aw0=$learning_rate->nilai*$b_arr[$not])
            @php($aw0_arr[$not]=$aw0)
    <!-- LANGKAH 8 -->
        @for($i=0; $i < $hl->nilai; $i++)
            @php($bn=$b_arr[$not]*$boo_arr[$i])
            @php($bn_arr[$not][]=$bn)
        @endfor
     <!-- LANGKAH 9 -->
        @for($i=0; $i < $hl->nilai; $i++)
                @php($bj=$bn_arr[$not][$i]*$fz_arr[$not][$i]*(1-$fz_arr[$not][$i]))
                @php($bj_arr[$not][]=$bj)
        @endfor
     <!-- LANGKAH 10 -->
            @php($bantu=0)
            @php($s=0)
            @for($r=0; $r < count($bobot_i_arr); $r++)
                    @php($av_arr[$not][]=$learning_rate->nilai*$bj_arr[$not][$s]*$xa[$bantu][$not])
            @php($s++)
            @php($bantu++)
            @if($bantu==11)
                @php($bantu=0)
            @endif
            @if($s==$hl->nilai)
                @php($s=0)
            @endif
        @endfor
     <!-- LANGKAH 11 -->
        @for($i=0; $i < $hl->nilai; $i++)
                @php($avb=$bj_arr[$not][$i]*$learning_rate->nilai)
                @php($avb_arr[$not][]=$avb)
        @endfor
     <!-- LANGKAH 12 -->
            @php($bantu=0)
            @php($s=0)
            @for($r=0; $r < count($bobot_i_arr); $r++)
                @php($bv=$av_arr[$not][$r]+$boi_arr[$r])
                @php($bv_arr[$not][]=$bv)
                @php($boi_arr[$r]=$bv)
            @endfor
     <!-- LANGKAH 13 -->
        @for($r=0; $r < $hl->nilai; $r++)
            @php($vn=$bi_arr[$r]-$avb_arr[$not][$r])
            @php($vn_arr[$not][]=$vn)
            @php($bi_arr[$r]=$vn)
        @endfor
     <!-- LANGKAH 14 -->
        @for($r=0; $r < $hl->nilai; $r++)
                    @php($wb= $boo_arr[$r]+$aw_arr[$not][$r])
                    @php($wb_arr[$not][]=$wb)
                    @php($boo_arr[$r]=abs($wb))
        @endfor
     <!-- LANGKAH 14 -->
                @if($not==0)
                @php($awn0_arr[$not]=$aw0_arr[$not]+$bo_arr[$not])
                @php($bo_arr[$not]=$aw0_arr[$not]+$bo_arr[$not])
                @else
                @php($awn0_arr[$not]=$aw0_arr[$not]+$awn0_arr[$not-1])
                @php($bo_arr[$not]=$aw0_arr[$not]+$awn0_arr[$not-1])
                @endif
     <!-- LANGKAH 16 -->
            @php($err1=abs($xtarget[$not]-$fyn_arr[$not]))
            @php($err=pow($err1,2))
            @php($err_arr[$not]=$err)
     <!-- LANGKAH 17 -->
            @php($pe=abs(($xtarget[$not]-$fyn_arr[$not])/$xtarget[$not])*100)
            @php($pe_arr[$not]=$pe)
    @php($not++)
    @endforeach
     <!-- AKHIR -->
        <!-- SUM SE -->
                 @php($sumse=array_sum($err_arr))
                 @php($sumse_arr[$sk]=$sumse)
        <!-- COUNT -->
                 @php($not_arr[$sk]=$not)
        <!-- MSE -->
                 @php($mse=array_sum($err_arr)/$not)
                 @php($mse_arr[$sk]=$mse)
        <!-- SUM PE -->
                 @php($sumpe=array_sum($pe_arr))
                 @php($sumpe_arr[$sk]=$sumpe)
        <!-- MA PE -->
                 @php($mape=array_sum($pe_arr)/$not)
                 @php($mape_arr[$sk]=$mape)
                 
        
    @endfor
    <br>
    <table border="1">
        <tr>
           <td>PERULANGAN</td>
           <td>SUM</td>
           <td>COUNT</td>
           <td>MSE</td>
           <td>SUM PE</td>
           <td>MA PE</td>
       </tr>
       @for($i=0; $i<$batas ; $i++)
       <tr>
           <td>{{$i+1}}</td>
           <td>{{$sumse_arr[$i]}}</td>
           <td>{{$not_arr[$i]}}</td>
           <td>{{$mse_arr[$i]}}</td>
           <td>{{$sumpe_arr[$i]}}</td>
           <td>{{$mape_arr[$i]}}</td>
       </tr>
       @endfor
    </table>
    <br>
    <p>Bobot Input update</p>
    <table border="1">
            
            @php($s=0)
            @for($i=0; $i < 12; $i++)
                <tr>
                @for($j=0; $j < $hl->nilai; $j++)
                    <td>{{$boi_arr[$s]}}</td>
                    @php($m_bobot_i[$i][$j]=$boi_arr[$s])
                @php($s++)
                @endfor
                </tr>
            @endfor
    </table>
    <br>
    <p>Bobot Output update</p>
    <table border="1">
        <tr>
        @for($j=0; $j < $hl->nilai; $j++)
            <td>{{$boo_arr[$j]}}</td>
        @endfor
        </tr>
    </table>
    <br>
    <p>Bias Input update</p>
    <table border="1">
        <tr>
        @for($j=0; $j < $hl->nilai; $j++)
            <td>{{$bi_arr[$j]}}</td>
        @endfor
        </tr>
    </table>
    <br>
    <p>Bias Output update</p>
    <table border="1">
        <tr>
            <td>{{$bo_arr[$not-1]}}</td>
        </tr>
    </table>
    <br>
    <br>
</body>
</html>