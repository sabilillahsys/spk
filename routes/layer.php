<?php
Route::group(['middleware' => ['web','roles']],function(){
    Route::group(['roles'=>['Master']],function(){
        Route::group([
            'prefix' => 'layer','as' => 'layer.',
        ], function(){
            Route::get('/data_hidden_layer/{id}','LayerController@data_hidden_layer')->name('data_hidden_layer');
            Route::get('/random_hidden_layer/{id}','LayerController@random_hidden_layer')->name('random_hidden_layer');
            Route::get('/json_hidden_layer/{id}','LayerController@json_hidden_layer')->name('json_hidden_layer');
            Route::post('/input_hidden_layer','LayerController@input_hidden_layer')->name('input_hidden_layer');
            Route::post('/update_hidden_layer/{id}','LayerController@update_hidden_layer')->name('update_hidden_layer');
            Route::get('/delete_hidden_layer/{id}','LayerController@delete_hidden_layer')->name('delete_hidden_layer');

            Route::get('/data_bobot_input/{id}','LayerController@data_bobot_input')->name('data_bobot_input');
            Route::get('/random_bobot_input/{id}','LayerController@random_bobot_input')->name('random_bobot_input');
            Route::get('/json_bobot_input/{id}','LayerController@json_bobot_input')->name('json_bobot_input');
            Route::post('/input_bobot_input','LayerController@input_bobot_input')->name('input_bobot_input');
            Route::post('/update_bobot_input/{id}','LayerController@update_bobot_input')->name('update_bobot_input');
            Route::get('/delete_bobot_input/{id}','LayerController@delete_bobot_input')->name('delete_bobot_input');

            Route::get('/data_bias_input/{id}','LayerController@data_bias_input')->name('data_bias_input');
            Route::get('/random_bias_input/{id}','LayerController@random_bias_input')->name('random_bias_input');
            Route::get('/json_bias_input/{id}','LayerController@json_bias_input')->name('json_bias_input');
            Route::post('/input_bias_input','LayerController@input_bias_input')->name('input_bias_input');
            Route::post('/update_bias_input/{id}','LayerController@update_bias_input')->name('update_bias_input');
            Route::get('/delete_bias_input/{id}','LayerController@delete_bias_input')->name('delete_bias_input');

            Route::get('/data_bobot_output/{id}','LayerController@data_bobot_output')->name('data_bobot_output');
            Route::get('/random_bobot_output/{id}','LayerController@random_bobot_output')->name('random_bobot_output');
            Route::get('/json_bobot_output/{id}','LayerController@json_bobot_output')->name('json_bobot_output');
            Route::post('/input_bobot_output','LayerController@input_bobot_output')->name('input_bobot_output');
            Route::post('/update_bobot_output/{id}','LayerController@update_bobot_output')->name('update_bobot_output');
            Route::get('/delete_bobot_output/{id}','LayerController@delete_bobot_output')->name('delete_bobot_output');

            Route::get('/data_bias_output/{id}','LayerController@data_bias_output')->name('data_bias_output');
            Route::get('/json_bias_output/{id}','LayerController@json_bias_output')->name('json_bias_output');
            Route::post('/input_bias_output','LayerController@input_bias_output')->name('input_bias_output');
            Route::post('/update_bias_output/{id}','LayerController@update_bias_output')->name('update_bias_output');
            Route::get('/delete_bias_output/{id}','LayerController@delete_bias_output')->name('delete_bias_output');

            Route::get('/data_learning_rate/{id}','LayerController@data_learning_rate')->name('data_learning_rate');
            Route::get('/json_learning_rate/{id}','LayerController@json_learning_rate')->name('json_learning_rate');
            Route::post('/input_learning_rate','LayerController@input_learning_rate')->name('input_learning_rate');
            Route::post('/update_learning_rate/{id}','LayerController@update_learning_rate')->name('update_learning_rate');
            Route::get('/delete_learning_rate/{id}','LayerController@delete_learning_rate')->name('delete_learning_rate');
            
        });
    });
});