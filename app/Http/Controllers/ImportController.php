<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\log;
use Auth;
use DataTables;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Hash;
use App\Imports\PenjualanImport;
class ImportController extends Controller
{
    public function import_penjualan(Request $request,$id)
    {
       
		// menangkap file excel
            $uploaded = $request->file('file');
            $nama_file=$uploaded->getClientOriginalName();
            $path = $uploaded->store('public/file_import');
		//dd($path);
 
		// import data
		Excel::import(new PenjualanImport($id), $path);
 
		return redirect()->back()->with('success', 'Berhasil Menambahkan');
    }
}
