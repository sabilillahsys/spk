<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Produk</title>
    <style>
        table {
            border-collapse: collapse;
        }
        td {
            text-align: center;
        }
        table {
            width: 100%;
        }

        th {
            width: 70px;
        }
    </style>
</head>
<body>
    <table border="1">
        <thead>
            <th>Jan</th>
            <th>Feb</th>
            <th>Mar</th>
            <th>Apr</th>
            <th>Mei</th>
            <th>Jun</th>
            <th>Jul</th>
            <th>Agu</th>
            <th>Sep</th>
            <th>Okt</th>
            <th>Nov</th>
            <th>Des</th>
        </thead>
        <tbody>
        <tr>
            @foreach($tahun as $tahuns)
                <tr>
                    @foreach($data as $datas)
                        @if($datas->tahun==$tahuns->tahun)
                            <td>{{$datas->kuantitas}}</td>
                        @endif
                    @endforeach
                </tr>
            @endforeach
            </tr>
        </tbody>
    </table>
</body>
</html>