<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Produk</title>
    <style>
        table {
            border-collapse: collapse;
        }
        td {
            text-align: center;
        }
        table {
            width: 100%;
        }

        th {
            width: 70px;
        }
    </style>
</head>
<body>
    <table border="1">
        <thead>
            <th>P</th>
            <th>x1</th>
            <th>x2</th>
            <th>x3</th>
            <th>x4</th>
            <th>x5</th>
            <th>x6</th>
            <th>x7</th>
            <th>x8</th>
            <th>x9</th>
            <th>x10</th>
            <th>x11</th>
            <th>x12</th>
            <th>T</th>
        </thead>
        <tbody>
            @php($no=1)
            @foreach($dataset as $datas)
                <tr>
                    <td>{{$no}}</td>
                    <td>{{$datas->x1}}</td>
                    <td>{{$datas->x2}}</td>
                    <td>{{$datas->x3}}</td>
                    <td>{{$datas->x4}}</td>
                    <td>{{$datas->x5}}</td>
                    <td>{{$datas->x6}}</td>
                    <td>{{$datas->x7}}</td>
                    <td>{{$datas->x8}}</td>
                    <td>{{$datas->x9}}</td>
                    <td>{{$datas->x10}}</td>
                    <td>{{$datas->x11}}</td>
                    <td>{{$datas->x12}}</td>
                    <td>{{$datas->target}}</td>
                </tr>
            @php($no++)
            @endforeach
        </tbody>
    </table>
</body>
</html>