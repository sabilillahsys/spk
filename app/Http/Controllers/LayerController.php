<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\hidden_layer;
use App\bobot_input;
use App\bobot_output;
use App\bias_input;
use App\bias_output;
use App\learning_rate;
use App\log;
use Auth;
use DataTables;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Hash;
class LayerController extends Controller
{
//START HIDDEN LAYER
    public function data_hidden_layer($id)
    {
        return view('layer.data_hidden_layer',compact('id'));
    }
    public function json_hidden_layer($id)
    {
        $data=hidden_layer::with(['produk'])->where('produk_id','=',$id)->get();
        //dd($data->toArray());
        return Datatables::of($data)
                    ->addIndexColumn()
                    ->addColumn('action', function ($user) {
                        return '
                        <button type="button" data-toggle="modal" data-target="#update'.$user->id.'" class="btn btn-primary"><i class="fa fa-edit" aria-hidden="true"></i> Update</button>
                        <!-- Modal-->
                        <div id="update'.$user->id.'" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" class="modal fade text-left">
                            <div role="document" class="modal-dialog">
                                <div class="modal-content">
                                
                                <div class="modal-body">
                                <form class="form-horizontal" action="'.route('layer.update_hidden_layer',$user->id).'" method="post" enctype="multipart/form-data">
                                <input type="hidden" name="_token" value="'.csrf_token().'">
                                <div class="row">
                                    <div class="col-md-12">
                                        <label><b>Nilai*</b></label>
                                        <fieldset class="form-group position-relative has-icon-left">
                                            <input type="number" class="form-control" name="nilai" placeholder="Nilai" value="'.$user->nilai.'">
                                            <div class="form-control-position">
                                            </div>
                                        </fieldset>
                                    </div>
                                    </div>
                                <div class="row">
                                    <div class="col-md-12 pt-2">
                                        <fieldset class="form-group position-relative has-icon-left">
                                            <button type="submit" class="btn btn-outline-primary btn-block"><i class="far fa-check-square"></i> Verifikasi</button>
                                        </fieldset>
                                    </div>
                                </div>
                            </form>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" data-dismiss="modal" class="btn btn-xs btn-info">Batal</button>
                                </div>
                                </div>
                            </div>
                            </div>
                            <!-- end of modal -->
                    <button type="button" data-toggle="modal" data-target="#hapus'.$user->id.'" class="btn btn-warning"><i class="fa fa-trash" aria-hidden="true"></i> Hapus</button>
                    <!-- Modal-->
                    <div id="hapus'.$user->id.'" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" class="modal fade text-left">
                        <div role="document" class="modal-dialog">
                            <div class="modal-content">
                            
                            <div class="modal-body">
                                <h4>
                                Yakin Akan Mengapus <b>'.$user->produk->nama.'</b>?
                                </h4>
                            </div>
                            <div class="modal-footer">
                                <button type="button" data-dismiss="modal" class="btn btn-xs btn-info">Batal</button>
                                <a href="'.route('layer.delete_hidden_layer',$user->id).'" data-id="'.$user->id.'" class="btn btn-xs btn-danger"><i class="fa fa-trash" aria-hidden="true"></i> Hapus</a>
                                
                            </div>
                            </div>
                        </div>
                        </div>
                        <!-- end of modal -->
                        ';
                    })
                    ->addColumn('produk', function ($user) {
                        return $user->produk->nama;
                    })
                    ->escapeColumns([])
                    ->make(true);
    }

    public function input_hidden_layer(Request $request)
    {
        $request->validate([
        'nilai'=>['required'],
        ]);
        hidden_layer::create([
        'produk_id' => $request->get('produk_id'),
        'nilai' => $request->get('nilai'),
        ]);
        return redirect()->back()->with('success', 'Akun Berhasil Dibuat');
    }
    public function update_hidden_layer(Request $request, $id)
    {
    $data=hidden_layer::find($id);
    $data->nilai=$request->get('nilai');
    $data->save();
    return redirect()->back()->with('success', 'Akun Berhasil Update');
    }
    public function delete_hidden_layer($id)
    {
        $data=hidden_layer::find($id);
        $data->delete();
        return redirect()->back()->with('success', 'Berhasil dihapus');
    }
//END HIDDEN LAYER
//START BOBOT INPUT
    public function data_bobot_input($id)
    {
        return view('layer.data_bobot_input',compact('id'));
    }
    public function random_bobot_input(Request $request, $id)
    {
        $x=12;
        $y=(int) $request->get('h_layer');
        $fi=bobot_input::where('produk_id',$id)->delete();
        for ($i=0; $i < $x; $i++) { 
            for ($j=0; $j < $y; $j++) {
                bobot_input::create([
                    'produk_id' => $id,
                    'nilai' => rand(1,10)/10,
                    ]); 
            }
        }
        return redirect()->back()->with('success', 'Set Random Sukses');
    }
    public function json_bobot_input($id)
    {
        $data=bobot_input::with(['produk'])->where('produk_id','=',$id)->orderBy('id','DESC')->get();
        //dd($data->toArray());
        return Datatables::of($data)
                    ->addIndexColumn()
                    ->addColumn('action', function ($user) {
                        return '
                        <button type="button" data-toggle="modal" data-target="#update'.$user->id.'" class="btn btn-primary"><i class="fa fa-edit" aria-hidden="true"></i> Update</button>
                        <!-- Modal-->
                        <div id="update'.$user->id.'" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" class="modal fade text-left">
                            <div role="document" class="modal-dialog">
                                <div class="modal-content">
                                
                                <div class="modal-body">
                                <form class="form-horizontal" action="'.route('layer.update_bobot_input',$user->id).'" method="post" enctype="multipart/form-data">
                                <input type="hidden" name="_token" value="'.csrf_token().'">
                                <div class="row">
                                    <div class="col-md-12">
                                        <label><b>Nilai*</b></label>
                                        <fieldset class="form-group position-relative has-icon-left">
                                            <input type="number" class="form-control" name="nilai" placeholder="Nilai" value="'.$user->nilai.'">
                                            <div class="form-control-position">
                                            </div>
                                        </fieldset>
                                    </div>
                                    </div>
                                <div class="row">
                                    <div class="col-md-12 pt-2">
                                        <fieldset class="form-group position-relative has-icon-left">
                                            <button type="submit" class="btn btn-outline-primary btn-block"><i class="far fa-check-square"></i> Verifikasi</button>
                                        </fieldset>
                                    </div>
                                </div>
                            </form>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" data-dismiss="modal" class="btn btn-xs btn-info">Batal</button>
                                </div>
                                </div>
                            </div>
                            </div>
                            <!-- end of modal -->
                    <button type="button" data-toggle="modal" data-target="#hapus'.$user->id.'" class="btn btn-warning"><i class="fa fa-trash" aria-hidden="true"></i> Hapus</button>
                    <!-- Modal-->
                    <div id="hapus'.$user->id.'" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" class="modal fade text-left">
                        <div role="document" class="modal-dialog">
                            <div class="modal-content">
                            
                            <div class="modal-body">
                                <h4>
                                Yakin Akan Mengapus <b>'.$user->produk->nama.'</b>?
                                </h4>
                            </div>
                            <div class="modal-footer">
                                <button type="button" data-dismiss="modal" class="btn btn-xs btn-info">Batal</button>
                                <a href="'.route('layer.delete_bobot_input',$user->id).'" data-id="'.$user->id.'" class="btn btn-xs btn-danger"><i class="fa fa-trash" aria-hidden="true"></i> Hapus</a>
                                
                            </div>
                            </div>
                        </div>
                        </div>
                        <!-- end of modal -->
                        ';
                    })
                    ->addColumn('produk', function ($user) {
                        return $user->produk->nama;
                    })
                    ->escapeColumns([])
                    ->make(true);
    }

    public function input_bobot_input(Request $request)
    {
        $request->validate([
        'nilai'=>['required'],
        ]);
        bobot_input::create([
        'produk_id' => $request->get('produk_id'),
        'nilai' => $request->get('nilai'),
        ]);
        return redirect()->back()->with('success', 'Akun Berhasil Dibuat');
    }
    public function update_bobot_input(Request $request, $id)
    {
    $data=bobot_input::find($id);
    $data->nilai=$request->get('nilai');
    $data->save();
    return redirect()->back()->with('success', 'Akun Berhasil Update');
    }
    public function delete_bobot_input($id)
    {
        $data=bobot_input::find($id);
        $data->delete();
        return redirect()->back()->with('success', 'Berhasil dihapus');
    }
//END BOBOT INPUT
//START BOBOT OUTPUT

    public function data_bobot_output($id)
    {
        return view('layer.data_bobot_output',compact('id'));
    }
    public function random_bobot_output(Request $request, $id)
    {
        $x=12;
        $y=(int) $request->get('h_layer');
        $fi=bobot_output::where('produk_id',$id)->delete();
            for ($j=0; $j < $y; $j++) {
                bobot_output::create([
                    'produk_id' => $id,
                    'nilai' => rand(1,10)/10,
                    ]); 
            }
        return redirect()->back()->with('success', 'Berhasil dihapus');
    }
    public function json_bobot_output($id)
    {
        $data=bobot_output::with(['produk'])->where('produk_id','=',$id)->get();
        //dd($data->toArray());
        return Datatables::of($data)
                    ->addIndexColumn()
                    ->addColumn('action', function ($user) {
                        return '
                        <button type="button" data-toggle="modal" data-target="#update'.$user->id.'" class="btn btn-primary"><i class="fa fa-edit" aria-hidden="true"></i> Update</button>
                        <!-- Modal-->
                        <div id="update'.$user->id.'" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" class="modal fade text-left">
                            <div role="document" class="modal-dialog">
                                <div class="modal-content">
                                
                                <div class="modal-body">
                                <form class="form-horizontal" action="'.route('layer.update_bobot_output',$user->id).'" method="post" enctype="multipart/form-data">
                                <input type="hidden" name="_token" value="'.csrf_token().'">
                                 <div class="row">
                                     <div class="col-md-12">
                                         <label><b>Nilai*</b></label>
                                         <fieldset class="form-group position-relative has-icon-left">
                                             <input type="number" class="form-control" name="nilai" placeholder="Nilai" value="'.$user->nilai.'">
                                             <div class="form-control-position">
                                             </div>
                                         </fieldset>
                                     </div>
                                     </div>
                                 <div class="row">
                                    <div class="col-md-12 pt-2">
                                        <fieldset class="form-group position-relative has-icon-left">
                                            <button type="submit" class="btn btn-outline-primary btn-block"><i class="far fa-check-square"></i> Verifikasi</button>
                                        </fieldset>
                                    </div>
                                </div>
                            </form>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" data-dismiss="modal" class="btn btn-xs btn-info">Batal</button>
                                </div>
                                </div>
                            </div>
                            </div>
                            <!-- end of modal -->
                    <button type="button" data-toggle="modal" data-target="#hapus'.$user->id.'" class="btn btn-warning"><i class="fa fa-trash" aria-hidden="true"></i> Hapus</button>
                    <!-- Modal-->
                    <div id="hapus'.$user->id.'" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" class="modal fade text-left">
                        <div role="document" class="modal-dialog">
                            <div class="modal-content">
                            
                            <div class="modal-body">
                                <h4>
                                Yakin Akan Mengapus <b>'.$user->produk->nama.'</b>?
                                </h4>
                            </div>
                            <div class="modal-footer">
                                <button type="button" data-dismiss="modal" class="btn btn-xs btn-info">Batal</button>
                                <a href="'.route('layer.delete_bobot_output',$user->id).'" data-id="'.$user->id.'" class="btn btn-xs btn-danger"><i class="fa fa-trash" aria-hidden="true"></i> Hapus</a>
                                
                            </div>
                            </div>
                        </div>
                        </div>
                        <!-- end of modal -->
                        ';
                    })
                    ->addColumn('produk', function ($user) {
                        return $user->produk->nama;
                    })
                    ->escapeColumns([])
                    ->make(true);
    }
    
    public function input_bobot_output(Request $request)
    {
        $request->validate([
        'nilai'=>['required'],
        ]);
        bobot_output::create([
        'produk_id' => $request->get('produk_id'),
        'nilai' => $request->get('nilai'),
        ]);
        return redirect()->back()->with('success', 'Akun Berhasil Dibuat');
    }
    public function update_bobot_output(Request $request, $id)
    {
        $data=bobot_output::find($id);
        $data->nilai=$request->get('nilai');
        $data->save();
        return redirect()->back()->with('success', 'Akun Berhasil Update');
    }
    public function delete_bobot_output($id)
    {
        $data=bobot_output::find($id);
        $data->delete();
        return redirect()->back()->with('success', 'Berhasil dihapus');
    }
//END BOBOT OUTPUT
//START BIAS INPUT
    public function data_bias_input($id)
    {
        return view('layer.data_bias_input',compact('id'));
    }
    public function random_bias_input(Request $request, $id)
    {
        $x=12;
        $y=(int) $request->get('h_layer');
        $fi=bias_input::where('produk_id',$id)->delete();
            for ($j=0; $j < $y; $j++) {
                bias_input::create([
                    'produk_id' => $id,
                    'nilai' => rand(1,10)/10,
                    ]); 
            }
        return redirect()->back()->with('success', 'Set Random Sukses');
    }
    public function json_bias_input($id)
    {
        $data=bias_input::with(['produk'])->where('produk_id','=',$id)->get();
        //dd($data->toArray());
        return Datatables::of($data)
                    ->addIndexColumn()
                    ->addColumn('action', function ($user) {
                        return '
                        <button type="button" data-toggle="modal" data-target="#update'.$user->id.'" class="btn btn-primary"><i class="fa fa-edit" aria-hidden="true"></i> Update</button>
                        <!-- Modal-->
                        <div id="update'.$user->id.'" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" class="modal fade text-left">
                            <div role="document" class="modal-dialog">
                                <div class="modal-content">
                                
                                <div class="modal-body">
                                <form class="form-horizontal" action="'.route('layer.update_bias_input',$user->id).'" method="post" enctype="multipart/form-data">
                                <input type="hidden" name="_token" value="'.csrf_token().'">
                                <div class="row">
                                    <div class="col-md-12">
                                        <label><b>Nilai*</b></label>
                                        <fieldset class="form-group position-relative has-icon-left">
                                            <input type="number" class="form-control" name="nilai" placeholder="Nilai" value="'.$user->nilai.'">
                                            <div class="form-control-position">
                                            </div>
                                        </fieldset>
                                    </div>
                                    </div>
                                <div class="row">
                                    <div class="col-md-12 pt-2">
                                        <fieldset class="form-group position-relative has-icon-left">
                                            <button type="submit" class="btn btn-outline-primary btn-block"><i class="far fa-check-square"></i> Verifikasi</button>
                                        </fieldset>
                                    </div>
                                </div>
                            </form>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" data-dismiss="modal" class="btn btn-xs btn-info">Batal</button>
                                </div>
                                </div>
                            </div>
                            </div>
                            <!-- end of modal -->
                    <button type="button" data-toggle="modal" data-target="#hapus'.$user->id.'" class="btn btn-warning"><i class="fa fa-trash" aria-hidden="true"></i> Hapus</button>
                    <!-- Modal-->
                    <div id="hapus'.$user->id.'" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" class="modal fade text-left">
                        <div role="document" class="modal-dialog">
                            <div class="modal-content">
                            
                            <div class="modal-body">
                                <h4>
                                Yakin Akan Mengapus <b>'.$user->produk->nama.'</b>?
                                </h4>
                            </div>
                            <div class="modal-footer">
                                <button type="button" data-dismiss="modal" class="btn btn-xs btn-info">Batal</button>
                                <a href="'.route('layer.delete_bias_input',$user->id).'" data-id="'.$user->id.'" class="btn btn-xs btn-danger"><i class="fa fa-trash" aria-hidden="true"></i> Hapus</a>
                                
                            </div>
                            </div>
                        </div>
                        </div>
                        <!-- end of modal -->
                        ';
                    })
                    ->addColumn('produk', function ($user) {
                        return $user->produk->nama;
                    })
                    ->escapeColumns([])
                    ->make(true);
    }

    public function input_bias_input(Request $request)
    {
        $request->validate([
        'nilai'=>['required'],
        ]);
        bias_input::create([
        'produk_id' => $request->get('produk_id'),
        'nilai' => $request->get('nilai'),
        ]);
        return redirect()->back()->with('success', 'Akun Berhasil Dibuat');
    }
    public function update_bias_input(Request $request, $id)
    {
        $data=bias_input::find($id);
        $data->nilai=$request->get('nilai');
        $data->save();
        return redirect()->back()->with('success', 'Akun Berhasil Update');
    }
    public function delete_bias_input($id)
    {
        $data=bias_input::find($id);
        $data->delete();
        return redirect()->back()->with('success', 'Berhasil dihapus');
    }
//END BIAS INPUT
    public function data_bias_output($id)
    {
        return view('layer.data_bias_output',compact('id'));
    }
    public function json_bias_output($id)
    {
        $data=bias_output::with(['produk'])->where('produk_id','=',$id)->get();
        //dd($data->toArray());
        return Datatables::of($data)
                    ->addIndexColumn()
                    ->addColumn('action', function ($user) {
                        return '
                        <button type="button" data-toggle="modal" data-target="#update'.$user->id.'" class="btn btn-primary"><i class="fa fa-edit" aria-hidden="true"></i> Update</button>
                        <!-- Modal-->
                        <div id="update'.$user->id.'" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" class="modal fade text-left">
                            <div role="document" class="modal-dialog">
                                <div class="modal-content">
                                
                                <div class="modal-body">
                                <form class="form-horizontal" action="'.route('layer.update_bias_output',$user->id).'" method="post" enctype="multipart/form-data">
                                <input type="hidden" name="_token" value="'.csrf_token().'">
                                <div class="row">
                                    <div class="col-md-12">
                                        <label><b>Nilai*</b></label>
                                        <fieldset class="form-group position-relative has-icon-left">
                                            <input type="number" class="form-control" name="nilai" placeholder="Nilai" value="'.$user->nilai.'">
                                            <div class="form-control-position">
                                            </div>
                                        </fieldset>
                                    </div>
                                    </div>
                                <div class="row">
                                    <div class="col-md-12 pt-2">
                                        <fieldset class="form-group position-relative has-icon-left">
                                            <button type="submit" class="btn btn-outline-primary btn-block"><i class="far fa-check-square"></i> Verifikasi</button>
                                        </fieldset>
                                    </div>
                                </div>
                            </form>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" data-dismiss="modal" class="btn btn-xs btn-info">Batal</button>
                                </div>
                                </div>
                            </div>
                            </div>
                            <!-- end of modal -->
                    <button type="button" data-toggle="modal" data-target="#hapus'.$user->id.'" class="btn btn-warning"><i class="fa fa-trash" aria-hidden="true"></i> Hapus</button>
                    <!-- Modal-->
                    <div id="hapus'.$user->id.'" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" class="modal fade text-left">
                        <div role="document" class="modal-dialog">
                            <div class="modal-content">
                            
                            <div class="modal-body">
                                <h4>
                                Yakin Akan Mengapus <b>'.$user->produk->nama.'</b>?
                                </h4>
                            </div>
                            <div class="modal-footer">
                                <button type="button" data-dismiss="modal" class="btn btn-xs btn-info">Batal</button>
                                <a href="'.route('layer.delete_bias_output',$user->id).'" data-id="'.$user->id.'" class="btn btn-xs btn-danger"><i class="fa fa-trash" aria-hidden="true"></i> Hapus</a>
                                
                            </div>
                            </div>
                        </div>
                        </div>
                        <!-- end of modal -->
                        ';
                    })
                    ->addColumn('produk', function ($user) {
                        return $user->produk->nama;
                    })
                    ->escapeColumns([])
                    ->make(true);
    }
    public function input_bias_output(Request $request)
    {
        $request->validate([
        'nilai'=>['required'],
        ]);
        bias_output::create([
        'produk_id' => $request->get('produk_id'),
        'nilai' => $request->get('nilai'),
        ]);
        return redirect()->back()->with('success', 'Akun Berhasil Dibuat');
    }
    public function update_bias_output(Request $request, $id)
    {
    $data=bias_output::find($id);
    $data->nilai=$request->get('nilai');
    $data->save();
    return redirect()->back()->with('success', 'Akun Berhasil Update');
    }
    public function delete_bias_output($id)
    {
        $data=bias_output::find($id);
        $data->delete();
        return redirect()->back()->with('success', 'Berhasil dihapus');
    }
    public function data_learning_rate($id)
        {
            return view('layer.data_learning_rate',compact('id'));
        }
    public function json_learning_rate($id)
    {
    $data=learning_rate::with(['produk'])->where('produk_id','=',$id)->get();
    //dd($data->toArray());
    return Datatables::of($data)
                ->addIndexColumn()
                ->addColumn('action', function ($user) {
                    return '
                    <button type="button" data-toggle="modal" data-target="#update'.$user->id.'" class="btn btn-primary"><i class="fa fa-edit" aria-hidden="true"></i> Update</button>
                    <!-- Modal-->
                    <div id="update'.$user->id.'" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" class="modal fade text-left">
                        <div role="document" class="modal-dialog">
                            <div class="modal-content">
                            
                            <div class="modal-body">
                            <form class="form-horizontal" action="'.route('layer.update_learning_rate',$user->id).'" method="post" enctype="multipart/form-data">
                            <input type="hidden" name="_token" value="'.csrf_token().'">
                             <div class="row">
                                 <div class="col-md-12">
                                     <label><b>Nilai*</b></label>
                                     <fieldset class="form-group position-relative has-icon-left">
                                         <input type="number" class="form-control" name="nilai" placeholder="Nilai" value="'.$user->nilai.'">
                                         <div class="form-control-position">
                                         </div>
                                     </fieldset>
                                 </div>
                                 </div>
                             <div class="row">
                                <div class="col-md-12 pt-2">
                                    <fieldset class="form-group position-relative has-icon-left">
                                        <button type="submit" class="btn btn-outline-primary btn-block"><i class="far fa-check-square"></i> Verifikasi</button>
                                    </fieldset>
                                </div>
                            </div>
                        </form>
                            </div>
                            <div class="modal-footer">
                                <button type="button" data-dismiss="modal" class="btn btn-xs btn-info">Batal</button>
                            </div>
                            </div>
                        </div>
                        </div>
                        <!-- end of modal -->
                <button type="button" data-toggle="modal" data-target="#hapus'.$user->id.'" class="btn btn-warning"><i class="fa fa-trash" aria-hidden="true"></i> Hapus</button>
                <!-- Modal-->
                <div id="hapus'.$user->id.'" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" class="modal fade text-left">
                    <div role="document" class="modal-dialog">
                        <div class="modal-content">
                        
                        <div class="modal-body">
                            <h4>
                            Yakin Akan Mengapus <b>'.$user->produk->nama.'</b>?
                            </h4>
                        </div>
                        <div class="modal-footer">
                            <button type="button" data-dismiss="modal" class="btn btn-xs btn-info">Batal</button>
                            <a href="'.route('layer.delete_learning_rate',$user->id).'" data-id="'.$user->id.'" class="btn btn-xs btn-danger"><i class="fa fa-trash" aria-hidden="true"></i> Hapus</a>
                            
                        </div>
                        </div>
                    </div>
                    </div>
                    <!-- end of modal -->
                    ';
                })
                ->addColumn('produk', function ($user) {
                    return $user->produk->nama;
                })
                ->escapeColumns([])
                ->make(true);
}

public function input_learning_rate(Request $request)
{
    $request->validate([
    'nilai'=>['required'],
    ]);
    learning_rate::create([
    'produk_id' => $request->get('produk_id'),
    'nilai' => $request->get('nilai'),
    ]);
    return redirect()->back()->with('success', 'Akun Berhasil Dibuat');
}
public function update_learning_rate(Request $request, $id)
    {
    $data=learning_rate::find($id);
    $data->nilai=$request->get('nilai');
    $data->save();
    return redirect()->back()->with('success', 'Akun Berhasil Update');
    }
public function delete_learning_rate($id)
{
    $data=learning_rate::find($id);
    $data->delete();
    return redirect()->back()->with('success', 'Berhasil dihapus');
    }
}
