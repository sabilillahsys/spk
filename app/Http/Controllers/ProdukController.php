<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\produk;
use App\detail_penjualan_produk;
use App\log;
use Auth;
use DataTables;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Hash;
class ProdukController extends Controller
{
    public function data_produk()
{
    return view('produk.data_produk');
}
public function json_produk()
{
    $data=produk::all();
    //dd($data->toArray());
    return Datatables::of($data)
                ->addIndexColumn()
                ->addColumn('action', function ($user) {
                    return '
                    <button type="button" data-toggle="modal" data-target="#update'.$user->id.'" class="btn btn-primary"><i class="fa fa-edit" aria-hidden="true"></i> Update</button>
                    <!-- Modal-->
                    <div id="update'.$user->id.'" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" class="modal fade text-left">
                        <div role="document" class="modal-dialog">
                            <div class="modal-content">
                            
                            <div class="modal-body">
                            <form class="form-horizontal" action="'.route('produk.update_produk',$user->id).'" method="post" enctype="multipart/form-data">
                            <input type="hidden" name="_token" value="'.csrf_token().'">
                             <div class="row">
                                 <div class="col-md-6">
                                     <label><b>Nama Produk*</b></label>
                                     <fieldset class="form-group position-relative has-icon-left">
                                         <input type="text" class="form-control" name="nama" placeholder="Nama Produk" value="'.$user->nama.'">
                                         <div class="form-control-position">
                                         <i class="fa fa-user-o" aria-hidden="true"></i>
                                         </div>
                                     </fieldset>
                                 </div>
                                 <div class="col-md-6">
                                     <label><b>Harga*</b></label>
                                     <fieldset class="form-group position-relative has-icon-left">
                                         <input type="number" class="form-control" name="harga" placeholder="Harga" required value="'.$user->harga.'">
                                         <div class="form-control-position">
                                         <i class="fa fa-whatsapp" aria-hidden="true"></i>
                                         </div>
                                     </fieldset>
                                 </div>
                                 <div class="col-md-6">
                                     <label><b>Jenis*</b></label>
                                     <fieldset class="form-group position-relative has-icon-left">
                                         <input type="text" class="form-control" name="jenis" placeholder="Jenis" value="'.$user->jenis.'">
                                         <div class="form-control-position">
                                         <i class="fa fa-user-o" aria-hidden="true"></i>
                                         </div>
                                     </fieldset>
                                 </div>
                                 <div class="col-md-6">
                                     <label><b>Jumlah*</b></label>
                                     <fieldset class="form-group position-relative has-icon-left">
                                         <input type="number" class="form-control" name="jumlah" placeholder="Jumlah" required value="'.$user->jumlah.'">
                                         <div class="form-control-position">
                                         <i class="fa fa-whatsapp" aria-hidden="true"></i>
                                         </div>
                                     </fieldset>
                                 </div>
                                 </div>
                             <div class="row">
                                <div class="col-md-12 pt-2">
                                    <fieldset class="form-group position-relative has-icon-left">
                                        <button type="submit" class="btn btn-outline-primary btn-block"><i class="far fa-check-square"></i> Verifikasi</button>
                                    </fieldset>
                                </div>
                            </div>
                        </form>
                            </div>
                            <div class="modal-footer">
                                <button type="button" data-dismiss="modal" class="btn btn-xs btn-info">Batal</button>
                            </div>
                            </div>
                        </div>
                        </div>
                        <!-- end of modal -->
                <button type="button" data-toggle="modal" data-target="#hapus'.$user->id.'" class="btn btn-warning"><i class="fa fa-trash" aria-hidden="true"></i> Hapus</button>
                <!-- Modal-->
                <div id="hapus'.$user->id.'" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" class="modal fade text-left">
                    <div role="document" class="modal-dialog">
                        <div class="modal-content">
                        
                        <div class="modal-body">
                            <h4>
                            Yakin Akan Mengapus Produk <b>'.$user->nama.'</b>?
                            </h4>
                        </div>
                        <div class="modal-footer">
                            <button type="button" data-dismiss="modal" class="btn btn-xs btn-info">Batal</button>
                            <a href="'.route('produk.delete_produk',$user->id).'" data-id="'.$user->id.'" class="btn btn-xs btn-danger"><i class="fa fa-trash" aria-hidden="true"></i> Hapus</a>
                            
                        </div>
                        </div>
                    </div>
                    </div>
                    <!-- end of modal -->
                    ';
                })
                ->addColumn('detail', function ($user) {
                    return '<a href="'.route('produk.detail_penjualan_produk',[$user->id]).'"class="btn btn-xs btn-success">Detail Penjualan</a>';
                })
                ->addColumn('hidden_layer', function ($user) {
                    return '<a href="'.route('layer.data_hidden_layer',[$user->id]).'"class="btn btn-xs btn-warning">Hidden Layer</a>';
                })
                ->addColumn('bobot_input', function ($user) {
                    return '<a href="'.route('layer.data_bobot_input',[$user->id]).'"class="btn btn-xs btn-warning">Bobot Input</a>';
                })
                ->addColumn('o_layer', function ($user) {
                    return '<a href="'.route('layer.data_bias_input',[$user->id]).'"class="btn btn-xs btn-primary">Bias Input</a>';
                })
                ->addColumn('u_layer', function ($user) {
                    return '<a href="'.route('layer.data_bobot_output',[$user->id]).'"class="btn btn-xs btn-secondary">Bobot Output</a>';
                })
                ->addColumn('bias_output', function ($user) {
                    return '<a href="'.route('layer.data_bias_output',[$user->id]).'"class="btn btn-xs btn-info">Bias Output</a>';
                })
                ->addColumn('lr', function ($user) {
                    return '<a href="'.route('layer.data_learning_rate',[$user->id]).'"class="btn btn-xs btn-danger">Learning Rate</a>';
                })
                ->escapeColumns([])
                ->make(true);
}

public function input_produk(Request $request)
{
    $request->validate([
        'nama'=>['required'],
        'harga'=>['required'],
        'jenis'=>['required'],
        'jumlah'=>['required'],
        ]);
            produk::create([
            'nama' => $request->get('nama'),
            'harga' => $request->get('harga'), 
            'jenis' => $request->get('jenis'), 
            'jumlah' => $request->get('jumlah'), 
    ]);
    return redirect()->back()->with('success', 'Berhasil Dibuat');
}
public function update_produk(Request $request, $id)
    {
            $data=produk::find($id);
            $data->nama=$request->get('nama');
            $data->harga=$request->get('harga');
            $data->jenis=$request->get('jenis');
            $data->jumlah=$request->get('jumlah');
            $data->save();
            return redirect()->back()->with('success', 'Berhasil Update');
    }
public function delete_produk($id)
{
    $data=produk::find($id);
    $data->delete();
    return redirect()->back()->with('success', 'Berhasil dihapus');
    }
    public function api()
    {
        $data=produk::all();
        return response()->json($data);
    }
    public function detail_penjualan_produk($id)
{
    return view('produk.detail_penjualan_produk',compact('id'));
}
public function json_penjualan_produk($id)
{
    $data=detail_penjualan_produk::with(['produk'])->where('produk_id','=',$id)->get();
    //dd($data->toArray());
    return Datatables::of($data)
                ->addIndexColumn()
                ->addColumn('action', function ($user) {
                    return '
                    <button type="button" data-toggle="modal" data-target="#update'.$user->id.'" class="btn btn-primary"><i class="fa fa-edit" aria-hidden="true"></i> Update</button>
                    <!-- Modal-->
                    <div id="update'.$user->id.'" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" class="modal fade text-left">
                        <div role="document" class="modal-dialog">
                            <div class="modal-content">
                            
                            <div class="modal-body">
                            <form class="form-horizontal" action="'.route('produk.update_penjualan_produk',$user->id).'" method="post" enctype="multipart/form-data">
                            <input type="hidden" name="_token" value="'.csrf_token().'">
                             <div class="row">
                                 <div class="col-md-6">
                                     <label><b>Kuantitas*</b></label>
                                     <fieldset class="form-group position-relative has-icon-left">
                                         <input type="text" class="form-control" name="kuantitas" placeholder="Kuantitas" value="'.$user->kuantitas.'">
                                         <div class="form-control-position">
                                         <i class="fa fa-user-o" aria-hidden="true"></i>
                                         </div>
                                     </fieldset>
                                 </div>
                                 <div class="col-md-6">
                                                    <label><b>Bulan *</b></label>
                                                    <fieldset class="form-group position-relative has-icon-left">
                                                        <select class="form-control" name="bulan"  required>
                                                            <option value="01" >Januari</option>
                                                            <option value="02" >Februari</option>
                                                            <option value="03" >Maret</option>
                                                            <option value="04" >April</option>
                                                            <option value="05" >Mei</option>
                                                            <option value="06" >Juni</option>
                                                            <option value="07" >Juli</option>
                                                            <option value="08" >Agustus</option>
                                                            <option value="09" >September</option>
                                                            <option value="10" >Oktober</option>
                                                            <option value="11" >November</option>
                                                            <option value="12" >Desember</option>
                                                        </select>
                                                        <div class="form-control-position">
                                                        </div>
                                                        </fieldset>
                                                        </div>
                                 <div class="col-md-6">
                                     <label><b>Tahun*</b></label>
                                     <fieldset class="form-group position-relative has-icon-left">
                                         <input type="number" class="form-control" name="tahun" value="'.$user->tahun.'">
                                         <div class="form-control-position">
                                         <i class="fa fa-user-o" aria-hidden="true"></i>
                                         </div>
                                     </fieldset>
                                 </div>
                                 <div class="col-md-6">
                                     <label><b>Total Harga*</b></label>
                                     <fieldset class="form-group position-relative has-icon-left">
                                         <input type="number" class="form-control" name="total_harga" placeholder="Total Harga" required value="'.$user->total_harga.'">
                                         <div class="form-control-position">
                                         <i class="fa fa-whatsapp" aria-hidden="true"></i>
                                         </div>
                                     </fieldset>
                                 </div>
                                 </div>
                             <div class="row">
                                <div class="col-md-12 pt-2">
                                    <fieldset class="form-group position-relative has-icon-left">
                                        <button type="submit" class="btn btn-outline-primary btn-block"><i class="far fa-check-square"></i> Verifikasi</button>
                                    </fieldset>
                                </div>
                            </div>
                        </form>
                            </div>
                            <div class="modal-footer">
                                <button type="button" data-dismiss="modal" class="btn btn-xs btn-info">Batal</button>
                            </div>
                            </div>
                        </div>
                        </div>
                        <!-- end of modal -->
                <button type="button" data-toggle="modal" data-target="#hapus'.$user->id.'" class="btn btn-warning"><i class="fa fa-trash" aria-hidden="true"></i> Hapus</button>
                <!-- Modal-->
                <div id="hapus'.$user->id.'" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" class="modal fade text-left">
                    <div role="document" class="modal-dialog">
                        <div class="modal-content">
                        
                        <div class="modal-body">
                            <h4>
                            Yakin Akan Mengapus Penjualan Produk <b>'.$user->produk->nama.'</b>?
                            </h4>
                        </div>
                        <div class="modal-footer">
                            <button type="button" data-dismiss="modal" class="btn btn-xs btn-info">Batal</button>
                            <a href="'.route('produk.delete_penjualan_produk',$user->id).'" data-id="'.$user->id.'" class="btn btn-xs btn-danger"><i class="fa fa-trash" aria-hidden="true"></i> Hapus</a>
                            
                        </div>
                        </div>
                    </div>
                    </div>
                    <!-- end of modal -->
                    ';
                })
                ->addColumn('produk', function ($user) {
                    return $user->produk->nama;
                })
                ->escapeColumns([])
                ->make(true);
}

public function input_penjualan_produk(Request $request)
{
    $request->validate([
        'kuantitas'=>['required'],
        'total_harga'=>['required'],
        ]);
            detail_penjualan_produk::create([
            'produk_id' => $request->get('produk_id'),
            'kuantitas' => $request->get('kuantitas'), 
            'bulan' => $request->get('bulan'), 
            'tahun' => $request->get('tahun'), 
            'total_harga' => $request->get('total_harga'), 
    ]);
    return redirect()->back()->with('success', 'Berhasil Dibuat');
}
public function update_penjualan_produk(Request $request, $id)
    {
            $data=detail_penjualan_produk::find($id);
            $data->kuantitas=$request->get('kuantitas');
            $data->bulan=$request->get('bulan');
            $data->bulan=$request->get('tahun');
            $data->total_harga=$request->get('total_harga');
            $data->save();
            return redirect()->back()->with('success', 'Berhasil Update');
    }
public function delete_penjualan_produk($id)
{
    $data=detail_penjualan_produk::find($id);
    $data->delete();
    return redirect()->back()->with('success', 'Berhasil dihapus');
    }
}
