<?php
Route::group(['middleware' => ['web','roles']],function(){
    Route::group(['roles'=>['Master','Pelanggan']],function(){
        Route::group([
            'prefix' => 'produk','as' => 'produk.',
        ], function(){
            Route::get('/api','ProdukController@api')->name('api');
            Route::get('/data_produk','ProdukController@data_produk')->name('data_produk');
            Route::get('/json_produk','ProdukController@json_produk')->name('json_produk');
            Route::post('/input_produk','ProdukController@input_produk')->name('input_produk');
            Route::post('/update_produk/{id}','ProdukController@update_produk')->name('update_produk');
            Route::get('/delete_produk/{id}','ProdukController@delete_produk')->name('delete_produk');

            Route::get('/detail_penjualan_produk/{id}','ProdukController@detail_penjualan_produk')->name('detail_penjualan_produk');
            Route::get('/json_penjualan_produk/{id}','ProdukController@json_penjualan_produk')->name('json_penjualan_produk');
            Route::post('/input_penjualan_produk','ProdukController@input_penjualan_produk')->name('input_penjualan_produk');
            Route::post('/update_penjualan_produk/{id}','ProdukController@update_penjualan_produk')->name('update_penjualan_produk');
            Route::get('/delete_penjualan_produk/{id}','ProdukController@delete_penjualan_produk')->name('delete_penjualan_produk');
        });
    });
});