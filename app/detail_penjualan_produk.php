<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;
class detail_penjualan_produk extends Model
{
    protected $fillable = [
        'produk_id', 
        'bulan',
        'tahun',
        'kuantitas',
        'total_harga', 
    ];
    public function produk()
    {
        return $this->belongsTo('App\produk','produk_id');
    }
    // public function gettanggalAttribute ()
    // {
    //     return Carbon::parse($this->attributes['tanggal'])
    //     ->translatedFormat('d F Y');
    // }
}
