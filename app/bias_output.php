<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class bias_output extends Model
{
    //
    protected $fillable = [
        'produk_id', 
        'nilai',
    ];
    public function produk()
    {
        return $this->belongsTo('App\produk','produk_id');
    }
}
