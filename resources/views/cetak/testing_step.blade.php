<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Produk</title>
    <style>
        table {
            border-collapse: collapse;
        }
        td {
            text-align: center;
        }
        table {
            width: 100%;
        }

        th {
            width: 70px;
        }
    </style>
</head>
<body>
        @php($gx=2)
        @php($no=0)
            @foreach($dataset as $datas)
            @php($d_x1[]=$datas->x1)
            @php($d_x2[]=$datas->x2)
            @php($d_x3[]=$datas->x3)
            @php($d_x4[]=$datas->x4)
            @php($d_x5[]=$datas->x5)
            @php($d_x6[]=$datas->x6)
            @php($d_x7[]=$datas->x7)
            @php($d_x8[]=$datas->x8)
            @php($d_x9[]=$datas->x9)
            @php($d_x10[]=$datas->x10)
            @php($d_x11[]=$datas->x11)
            @php($d_x12[]=$datas->x12)
        @php($no++)
            @php($d_target[]=$datas->target)
            @endforeach
   
            @php($nn=1)
            @foreach($dataset as $datas)
            @php($d_x1[]=$datas->x1)
            @php($d_x2[]=$datas->x2)
            @php($d_x3[]=$datas->x3)
            @php($d_x4[]=$datas->x4)
            @php($d_x5[]=$datas->x5)
            @php($d_x6[]=$datas->x6)
            @php($d_x7[]=$datas->x7)
            @php($d_x8[]=$datas->x8)
            @php($d_x9[]=$datas->x9)
            @php($d_x10[]=$datas->x10)
            @php($d_x11[]=$datas->x11)
            @php($d_x12[]=$datas->x12)
            @php($d_target[]=$datas->target)
                
                        @php($x1=0.8*($datas->x1-min($d_x1))/(max($d_x1)-min($d_x1))+0.1)
                        @php($xa[0][]=$x1)
                        @php($x2=0.8*($datas->x2-min($d_x2))/(max($d_x2)-min($d_x2))+0.1)
                        @php($xa[1][]=$x2)
                        @php($x3=0.8*($datas->x3-min($d_x3))/(max($d_x3)-min($d_x3))+0.1)
                        @php($xa[2][]=$x3)
                        @php($x4=0.8*($datas->x4-min($d_x4))/(max($d_x4)-min($d_x4))+0.1)
                        @php($xa[3][]=$x4)
                        @php($x5=0.8*($datas->x5-min($d_x5))/(max($d_x5)-min($d_x5))+0.1)
                        @php($xa[4][]=$x5)
                        @php($x6=0.8*($datas->x6-min($d_x6))/(max($d_x6)-min($d_x6))+0.1)
                        @php($xa[5][]=$x6)
                        @php($x7=0.8*($datas->x7-min($d_x7))/(max($d_x7)-min($d_x7))+0.1)
                        @php($xa[6][]=$x7)
                        @php($x8=0.8*($datas->x8-min($d_x8))/(max($d_x8)-min($d_x8))+0.1)
                        @php($xa[7][]=$x8)
                        @php($x9=0.8*($datas->x9-min($d_x9))/(max($d_x9)-min($d_x9))+0.1)
                        @php($xa[8][]=$x9)
                        @php($x10=0.8*($datas->x10-min($d_x10))/(max($d_x10)-min($d_x10))+0.1)
                        @php($xa[9][]=$x10)
                        @php($x11=0.8*($datas->x11-min($d_x11))/(max($d_x11)-min($d_x11))+0.1)
                        @php($xa[10][]=$x11)
                        @php($x12=0.8*($datas->x12-min($d_x12))/(max($d_x12)-min($d_x12))+0.1)
                        @php($xa[11][]=$x12)
                        @php($target=0.8*($datas->target-min($d_target))/(max($d_target)-min($d_target))+0.1)
                        @php($xtarget[]=$target)
            @php($nn++)
            @endforeach
            @foreach($bobot_i as $boi)
                @php($boi_arr[]= $boi->nilai)
            @endforeach
            @php($s=0)
            @for($i=0; $i < 12; $i++)
                @for($j=0; $j < $hl->nilai; $j++)
                    @php($m_bobot_i[$i][$j]=$boi_arr[$s])
                @php($s++)
                @endfor
            @endfor
   
        @foreach($bobot_o as $boo)
            @php( $boo_arr[]=$boo->nilai)
        @endforeach
        @foreach($bias_i as $bi)
            @php($bi_arr[]=$bi->nilai)
        @endforeach
       
        @php($bo_arr[]=$bias_o->nilai)
                
          
    @for($sk=0 ; $sk < $batas ; $sk++)
    @php($not=0)
    @foreach($dataset as $dat)
            @for($j=0; $j < $hl->nilai; $j++)  
                @php($sum=0)
                    @for($i=0; $i < 12; $i++)
                        @php($sum+=($m_bobot_i[$i][$j]*$xa[$i][$not]))
                    @endfor
                @php($z=$bi_arr[$j]+$sum)
                @php($z_arr[$not][]=$z)
             @endfor
    <!-- LANGKAH 2 -->
            @for($i=0; $i < $hl->nilai; $i++)
            
                @php($fz=1/(1+(pow(2.71828183,-$z_arr[$not][$i]))))
                @php($fz_arr[$not][]=$fz)
            
            @endfor
    <!-- LANGKAH 3 -->
            @php($yn=0)
            @for($k=0;$k<(count($boo_arr));$k++)
                @php($yn+=($fz_arr[$not][$k])*$boo_arr[$k])
            @endfor
            @if($not==0)
            @php($yn=$bo_arr[$not]+$yn)
            @else
            @php($yn=$bo_arr[$not-1]+$yn)
            @endif
            @php($yn_arr[$not]=$yn)
    <!-- LANGKAH 4 -->
            @php($fyn=1/(1+(pow(2.71828183,-$yn_arr[$not]))))
                @php($fyn_arr[$not]=$fyn)
    <!-- LANGKAH 5 -->
            @php($b=($xtarget[$not]-$fyn_arr[$not])*($fyn_arr[$not])*(1-$fyn_arr[$not]))
            @php($b_arr[$not]=$b)
    <!-- LANGKAH 6 -->
            @for($i=0; $i < $hl->nilai; $i++)
                @php($aw=$learning_rate->nilai*$b_arr[$not]*$fz_arr[$not][$i])
                @php($aw_arr[$not][]=$aw)
            @endfor
    <!-- LANGKAH 7 -->
            @php($aw0=$learning_rate->nilai*$b_arr[$not])
            @php($aw0_arr[$not]=$aw0)
    <!-- LANGKAH 8 -->
        @for($i=0; $i < $hl->nilai; $i++)
            @php($bn=$b_arr[$not]*$boo_arr[$i])
            @php($bn_arr[$not][]=$bn)
        @endfor
     <!-- LANGKAH 9 -->
        @for($i=0; $i < $hl->nilai; $i++)
                @php($bj=$bn_arr[$not][$i]*$fz_arr[$not][$i]*(1-$fz_arr[$not][$i]))
                @php($bj_arr[$not][]=$bj)
        @endfor
     <!-- LANGKAH 10 -->
            @php($bantu=0)
            @php($s=0)
            @for($r=0; $r < count($bobot_i_arr); $r++)
                    @php($av_arr[$not][]=$learning_rate->nilai*$bj_arr[$not][$s]*$xa[$bantu][$not])
            @php($s++)
            @php($bantu++)
            @if($bantu==11)
                @php($bantu=0)
            @endif
            @if($s==$hl->nilai)
                @php($s=0)
            @endif
        @endfor
     <!-- LANGKAH 11 -->
        @for($i=0; $i < $hl->nilai; $i++)
                @php($avb=$bj_arr[$not][$i]*$learning_rate->nilai)
                @php($avb_arr[$not][]=$avb)
        @endfor
     <!-- LANGKAH 12 -->
            @php($bantu=0)
            @php($s=0)
            @for($r=0; $r < count($bobot_i_arr); $r++)
                @php($bv=$av_arr[$not][$r]+$boi_arr[$r])
                @php($bv_arr[$not][]=$bv)
                @php($boi_arr[$r]=$bv)
            @endfor
     <!-- LANGKAH 13 -->
        @for($r=0; $r < $hl->nilai; $r++)
            @php($vn=$bi_arr[$r]-$avb_arr[$not][$r])
            @php($vn_arr[$not][]=$vn)
            @php($bi_arr[$r]=$vn)
        @endfor
     <!-- LANGKAH 14 -->
        @for($r=0; $r < $hl->nilai; $r++)
                    @php($wb= $boo_arr[$r]+$aw_arr[$not][$r])
                    @php($wb_arr[$not][]=$wb)
                    @php($boo_arr[$r]=abs($wb))
        @endfor
     <!-- LANGKAH 14 -->
                @if($not==0)
                @php($awn0_arr[$not]=$aw0_arr[$not]+$bo_arr[$not])
                @php($bo_arr[$not]=$aw0_arr[$not]+$bo_arr[$not])
                @else
                @php($awn0_arr[$not]=$aw0_arr[$not]+$awn0_arr[$not-1])
                @php($bo_arr[$not]=$aw0_arr[$not]+$awn0_arr[$not-1])
                @endif
     <!-- LANGKAH 16 -->
            @php($err1=abs($xtarget[$not]-$fyn_arr[$not]))
            @php($err=pow($err1,2))
            @php($err_arr[$not]=$err)
     <!-- LANGKAH 17 -->
            @php($pe=abs(($xtarget[$not]-$fyn_arr[$not])/$xtarget[$not])*100)
            @php($pe_arr[$not]=$pe)
    @php($not++)
    @endforeach
     <!-- AKHIR -->
        <!-- SUM SE -->
                 @php($sumse=array_sum($err_arr))
                 @php($sumse_arr[$sk]=$sumse)
        <!-- COUNT -->
                 @php($not_arr[$sk]=$not)
        <!-- MSE -->
                 @php($mse=array_sum($err_arr)/$not)
                 @php($mse_arr[$sk]=$mse)
        <!-- SUM PE -->
                 @php($sumpe=array_sum($pe_arr))
                 @php($sumpe_arr[$sk]=$sumpe)
        <!-- MA PE -->
                 @php($mape=array_sum($pe_arr)/$not)
                 @php($mape_arr[$sk]=$mape)
    @endfor
    <br>
    <table border="1">
        <tr>
           <td>PERULANGAN</td>
           <td>SUM</td>
           <td>COUNT</td>
           <td>MSE</td>
           <td>SUM PE</td>
           <td>MA PE</td>
       </tr>
       @for($i=0; $i<$batas ; $i++)
       <tr>
           <td>{{$i}}</td>
           <td>{{$sumse_arr[$i]}}</td>
           <td>{{$not_arr[$i]}}</td>
           <td>{{$mse_arr[$i]}}</td>
           <td>{{$sumpe_arr[$i]}}</td>
           <td>{{$mape_arr[$i]}}</td>
       </tr>
       @endfor
    </table>
    <h1>DATA TESTING</h1>
    <br>
    <table border="1">
        <thead>
            <th>P</th>
            <th>x1</th>
            <th>x2</th>
            <th>x3</th>
            <th>x4</th>
            <th>x5</th>
            <th>x6</th>
            <th>x7</th>
            <th>x8</th>
            <th>x9</th>
            <th>x10</th>
            <th>x11</th>
            <th>x12</th>
            <th>T</th>
        </thead>
        <tbody>
            @foreach($testing as $datas)
            @php($t_x1[]=$datas->x1)
            @php($t_x2[]=$datas->x2)
            @php($t_x3[]=$datas->x3)
            @php($t_x4[]=$datas->x4)
            @php($t_x5[]=$datas->x5)
            @php($t_x6[]=$datas->x6)
            @php($t_x7[]=$datas->x7)
            @php($t_x8[]=$datas->x8)
            @php($t_x9[]=$datas->x9)
            @php($t_x10[]=$datas->x10)
            @php($t_x11[]=$datas->x11)
            @php($t_x12[]=$datas->x12)
            @php($t_target[]=$datas->target)
                <tr>
                    <td>{{$no}}</td>
                    <td>{{$datas->x1}}</td>
                    <td>{{$datas->x2}}</td>
                    <td>{{$datas->x3}}</td>
                    <td>{{$datas->x4}}</td>
                    <td>{{$datas->x5}}</td>
                    <td>{{$datas->x6}}</td>
                    <td>{{$datas->x7}}</td>
                    <td>{{$datas->x8}}</td>
                    <td>{{$datas->x9}}</td>
                    <td>{{$datas->x10}}</td>
                    <td>{{$datas->x11}}</td>
                    <td>{{$datas->x12}}</td>
                    <td>{{$datas->target}}</td>
                </tr>
            @php($no++)
            @endforeach
            <tr>
                <td colspan="14"></td>
            </tr>
            <tr>
                <td><b>MIN</b></td>
                <td>{{min($t_x1)}}</td>
                <td>{{min($t_x2)}}</td>
                <td>{{min($t_x3)}}</td>
                <td>{{min($t_x4)}}</td>
                <td>{{min($t_x5)}}</td>
                <td>{{min($t_x6)}}</td>
                <td>{{min($t_x7)}}</td>
                <td>{{min($t_x8)}}</td>
                <td>{{min($t_x9)}}</td>
                <td>{{min($t_x10)}}</td>
                <td>{{min($t_x11)}}</td>
                <td>{{min($t_x12)}}</td>
                <td>{{min($t_target)}}</td>
            </tr>
            <tr>
                <td><b>MAX</b></td>
                <td>{{max($t_x1)}}</td>
                <td>{{max($t_x2)}}</td>
                <td>{{max($t_x3)}}</td>
                <td>{{max($t_x4)}}</td>
                <td>{{max($t_x5)}}</td>
                <td>{{max($t_x6)}}</td>
                <td>{{max($t_x7)}}</td>
                <td>{{max($t_x8)}}</td>
                <td>{{max($t_x9)}}</td>
                <td>{{max($t_x10)}}</td>
                <td>{{max($t_x11)}}</td>
                <td>{{max($t_x12)}}</td>
                <td>{{max($t_target)}}</td>
            </tr>
        </tbody>
    </table>
    <br>
    <br>
    <h3>Normalisasi</h3>
    <table border="1">
        <thead>
            <th>P</th>
            <th>x1</th>
            <th>x2</th>
            <th>x3</th>
            <th>x4</th>
            <th>x5</th>
            <th>x6</th>
            <th>x7</th>
            <th>x8</th>
            <th>x9</th>
            <th>x10</th>
            <th>x11</th>
            <th>x12</th>
            <th>T</th>
        </thead>
        <tbody>
            @foreach($testing as $datas)
            @php($t_x1[]=$datas->x1)
            @php($t_x2[]=$datas->x2)
            @php($t_x3[]=$datas->x3)
            @php($t_x4[]=$datas->x4)
            @php($t_x5[]=$datas->x5)
            @php($t_x6[]=$datas->x6)
            @php($t_x7[]=$datas->x7)
            @php($t_x8[]=$datas->x8)
            @php($t_x9[]=$datas->x9)
            @php($t_x10[]=$datas->x10)
            @php($t_x11[]=$datas->x11)
            @php($t_x12[]=$datas->x12)
            @php($t_target[]=$datas->target)
                <tr>
                    <td>{{$nn}}</td>
                    <td>
                        @php($x1=0.8*($datas->x1-min($t_x1))/(max($t_x1)-min($t_x1))+0.1)
                        {{$x1}}
                        @php($xn[0][]=$x1)
                    </td>
                    <td> @php($x2=0.8*($datas->x2-min($t_x2))/(max($t_x2)-min($t_x2))+0.1)
                        {{$x2}}
                        @php($xn[1][]=$x2)
                    </td>
                    <td> @php($x3=0.8*($datas->x3-min($t_x3))/(max($t_x3)-min($t_x3))+0.1)
                        {{$x3}}
                        @php($xn[2][]=$x3)
                    </td>
                    <td> @php($x4=0.8*($datas->x4-min($t_x4))/(max($t_x4)-min($t_x4))+0.1)
                        {{$x4}}
                        @php($xn[3][]=$x4)
                    </td>
                    <td> @php($x5=0.8*($datas->x5-min($t_x5))/(max($t_x5)-min($t_x5))+0.1)
                        {{$x5}}
                        @php($xn[4][]=$x5)
                    </td>
                    <td> @php($x6=0.8*($datas->x6-min($t_x6))/(max($t_x6)-min($t_x6))+0.1)
                        {{$x6}}
                        @php($xn[5][]=$x6)
                    </td>
                    <td> @php($x7=0.8*($datas->x7-min($t_x7))/(max($t_x7)-min($t_x7))+0.1)
                        {{$x7}}
                        @php($xn[6][]=$x7)
                    </td>
                    <td> @php($x8=0.8*($datas->x8-min($t_x8))/(max($t_x8)-min($t_x8))+0.1)
                        {{$x8}}
                        @php($xn[7][]=$x8)
                    </td>
                    <td> @php($x9=0.8*($datas->x9-min($t_x9))/(max($t_x9)-min($t_x9))+0.1)
                        {{$x9}}
                        @php($xn[8][]=$x9)
                    </td>
                    <td> @php($x10=0.8*($datas->x10-min($t_x10))/(max($t_x10)-min($t_x10))+0.1)
                        {{$x10}}
                        @php($xn[9][]=$x10)
                    </td>
                    <td> @php($x11=0.8*($datas->x11-min($t_x11))/(max($t_x11)-min($t_x11))+0.1)
                        {{$x11}}
                        @php($xn[10][]=$x11)
                    </td>
                    <td> @php($x12=0.8*($datas->x12-min($t_x12))/(max($t_x12)-min($t_x12))+0.1)
                        {{$x12}}
                        @php($xn[11][]=$x12)
                    </td>
                    <td>
                        @php($target=0.8*($datas->target-min($t_target))/(max($t_target)-min($t_target))+0.1)
                        {{$target}}
                        @php($xntarget[]=$target)
                    </td>
                </tr>
            @php($nn++)
            @endforeach
            
        </tbody>
    </table>
    <br>
    <p>Bobot Input update</p>
    <table border="1">
            
            @php($s=0)
            @for($i=0; $i < 12; $i++)
                <tr>
                @for($j=0; $j < $hl->nilai; $j++)
                    <td>{{$boi_arr[$s]}}</td>
                    @php($m_bobot_i[$i][$j]=$boi_arr[$s])
                @php($s++)
                @endfor
                </tr>
            @endfor
    </table>
    <br>
    <p>Bobot Output update</p>
    <table border="1">
        <tr>
        @for($j=0; $j < $hl->nilai; $j++)
            <td>{{$boo_arr[$j]}}</td>
        @endfor
        </tr>
    </table>
    <br>
    <p>Bias Input update</p>
    <table border="1">
        <tr>
        @for($j=0; $j < $hl->nilai; $j++)
            <td>{{$bi_arr[$j]}}</td>
        @endfor
        </tr>
    </table>
    <br>
    <p>Bias Output update</p>
    <table border="1">
        <tr>
            <td>{{$bo_arr[$not-1]}}</td>
        </tr>
    </table>
    <br>

    <p>PERHITUNGAN</p>
    <table border="1">
        <tr>
            <td>NO</td>
            <td colspan="{{$hl->nilai}}">Langkah 1</td>
            <td colspan="{{$hl->nilai}}">Langkah 2</td>
            <td>Langkah 3</td>
            <td>Langkah 4</td>
            <td>Langkah 5</td>
            <td>Langkah 6</td>
            <td>Langkah 7</td>
            
            
        </tr>
    @php($non=0)
    @foreach($testing as $dts)
        <tr>
            <td>{{$non+$not+1}}</td>
                @for($j=0; $j < $hl->nilai; $j++)  
                @php($sum=0)
                    @for($i=0; $i < 12; $i++)
                        @php($sum+=($m_bobot_i[$i][$j]*$xn[$i][$non]))
                    @endfor
                @php($z=$bi_arr[$j]+$sum)
                <td>{{$z}}</td>
                @php($zn_arr[$non][]=$z)
             @endfor
    <!-- LANGKAH 2 -->
            @for($i=0; $i < $hl->nilai; $i++)
            <td>
                @php($fzn=1/(1+(pow(2.71828183,-$zn_arr[$non][$i]))))
                {{$fzn}}
                @php($fzn_arr[$non][]=$fzn)
            </td>
            @endfor

    <!-- LANGKAH 3 -->
        <td>
            @php($yn=0)
            @for($k=0;$k<(count($boo_arr));$k++)
                @php($yn+=($fzn_arr[$non][$k]*$boo_arr[$k]))
            @endfor
            @if($non==0)
                @php($yn=$bo_arr[$non]+$yn)
            @else
                @php($yn=$bo_arr[$non-1]+$yn)
            @endif
                @php($ynn_arr[$non]=$yn)
            {{$yn}}
            </td>
    <!-- LANGKAH 4 -->
            <td>
            @php($fyn=1/(1+(pow(2.71828183,-$ynn_arr[$non]))))
                @php($fyn_arr[$non]=$fyn)
            {{$fyn}}
            </td>

    <!-- LANGKAH 5 -->
            <td>
                @php($err=($fyn_arr[$non]-$xntarget[$non]))
                @php($err=pow($err,2))
                {{$err}}
                @php($errn_arr[$non]=$err)
            </td>
    <!-- LANGKAH 6 -->
            <td>
                @php($dn=($fyn_arr[$non]*max($t_target)) - ($fyn_arr[$non]*min($t_target)) + min($t_target))
                @php($dn=round($dn,11))
                {{$dn}}
                @php($dn_arr[$non]=$dn)
            </td>
    <!-- LANGKAH 7 -->
            <td>
                @php($pen=abs(($xntarget[$non]-$fyn_arr[$non])/$xntarget[$non])*100)
                {{$pen}}
                @php($pen_arr[$non]=$pen)
            </td>
           
        </tr>
            @php($non++)
        @endforeach
    </table>
    <br>
    <table border="1">
        <tr>
            <td>SUM ERROR</td>
            <td>COUNT ERROR</td>
            <td>MSE</td>
            
            
        </tr>
        <tr>
            <td>{{array_sum($errn_arr)}}</td>
            <td>{{$non}}</td>
            <td>{{array_sum($errn_arr)/$non}}</td>
            
           
        </tr>
    </table>
    @for($prm=0 ; $prm < $jum_prediksi ; $prm++)
    <p>Bobot Input update Untuk Peramalan</p>
    <table border="1">
        @php($bzn0=0)
        @php($bzn1=0)
                @for($j=0; $j < count($boi_arr); $j++)
                    @php($boi_arr[$j]=$boi_arr[$j]+$zn_arr[$non-1+$prm][$bzn0]*0.01*$xn[$bzn1][$non-1])
                    @php($bzn0++)
                    @if($bzn0==$hl->nilai)
                        @php($bzn0=0)
                    @endif
                    @php($bzn1++)
                    @if($bzn1==12)
                        @php($bzn1=0)
                    @endif
                @endfor

            @php($s=0)
            @for($i=0; $i < 12; $i++)
                <tr>
                @for($j=0; $j < $hl->nilai; $j++)
                    <td>{{$boi_arr[$s]}}</td>
                    @php($r_bobot_i[$i][$j]=$boi_arr[$j])
                @php($s++)
                @endfor
                </tr>
            @endfor
    </table>
    <br>
    <p>Bobot Output update</p>
    <table border="1">
        <tr>
        @php($bzn0=0)
        @for($j=0; $j < $hl->nilai; $j++)
            @php($boo_arr[$j]=$boo_arr[$j]+$xntarget[$non-1]*0.01*$fzn_arr[$non-1+$prm][$j])
            <td>{{$boo_arr[$j]}}</td>
            @php($bzn0++)
            @if($bzn0==$hl->nilai)
                @php($bzn0=0)
            @endif
        @endfor
        </tr>
    </table>
    <br>
    <p>Bias Input update</p>
    <table border="1">
        <tr>
        @php($bzn0=0)
        @for($j=0; $j < $hl->nilai; $j++)
            @php($bi_arr[$j]=$bi_arr[$j]+$zn_arr[$non-1][$bzn0]*0.01)
            <td>{{$bi_arr[$j]}}</td>
            @php($bzn0++)
            @if($bzn0==$hl->nilai)
                @php($bzn0=0)
            @endif
        @endfor
        </tr>
    </table>
    <br>
    <p>Bias Output update</p>
    <table border="1">
        <tr>
            <td>
                @php($bo_arr[$not-1]=$bo_arr[$not-1]+$xntarget[$non-1]*0.01)
                {{$bo_arr[$not-1]}}
            </td>
        </tr>
    </table>
    <br>
    <p>Z Baru {{$non+$prm}}</p>
    <table border="1">
        <tr>
            <td colspan="{{$hl->nilai}}">Langkah 1</td>
            <td colspan="{{$hl->nilai}}">Langkah 2</td>
            <td>Langkah 3</td>
            <td>Langkah 4</td>
        </tr>
        <tr>
        @for($j=0; $j < $hl->nilai; $j++)  
                @php($sum=0)
                    @for($i=0; $i < 12; $i++)
                        @php($sum+=(($r_bobot_i[$i][$j]*$xn[$i][$non-1])))
                    @endfor
                @php($z=$bi_arr[$j]+$sum)
                <td>{{$z}}</td>
                @php($zn_arr[$non+$prm][]=$z)
             @endfor
             <!-- LANGKAH 2 -->
            @for($i=0; $i < $hl->nilai; $i++)
            <td>
                @php($fzn=1/(1+(pow(2.71828183,-$zn_arr[$non+$prm][$i]))))
                {{$fzn}}
                @php($fzn_arr[$non+$prm][$i]=$fzn)
            </td>
            @endfor
             <!-- LANGKAH 3 -->
        <td>
            @php($yn=0)
            @for($k=0;$k<(count($boo_arr));$k++)
                @php($yn+=($fzn_arr[$non+$prm][$k]*$boo_arr[$k]))
            @endfor
            @if($non==0)
                @php($yn=$bo_arr[$non+$prm]+$yn)
            @else
                @php($yn=$bo_arr[$non-1+$prm]+$yn)
            @endif
                @php($ynn_arr[$non+$prm]=$yn)
            {{$yn}}
            </td>
    <!-- LANGKAH 4 -->
            <td>
            @php($fyn=1/(1+(pow(2.71828183,-$ynn_arr[$non+$prm]))))
                @php($fyn_arr[$non+$prm]=$fyn)
            {{$fyn}}
            </td>
        </tr>
    </table>
    <br>
    <table border="1">
        <tr>
            <td>Tahun</td>
            <td>Jan</td>
            <td>Feb</td>
            <td>Mar</td>
            <td>Apr</td>
            <td>Mei</td>
            <td>Jun</td>
            <td>Jul</td>
            <td>Agu</td>
            <td>Sep</td>
            <td>Okt</td>
            <td>Nov</td>
            <td>Des</td>
        </tr>
    @for($prm=0 ; $prm < $jum_prediksi ; $prm++)
        <!-- BOBOT INPUT -->
        @php($bzn0=0)
        @php($bzn1=0)
                @for($j=0; $j < count($boi_arr); $j++)
                    @php($boi_arr[$j]=$boi_arr[$j]+$zn_arr[$non-1+$prm][$bzn0]*0.01*$xn[$bzn1][$non-1])
                    @php($bzn0++)
                    @if($bzn0==$hl->nilai)
                        @php($bzn0=0)
                    @endif
                    @php($bzn1++)
                    @if($bzn1==12)
                        @php($bzn1=0)
                    @endif
                @endfor
            @php($s=0)
            @for($i=0; $i < 12; $i++)
                @for($j=0; $j < $hl->nilai; $j++)
                    @php($r_bobot_i[$i][$j]=$boi_arr[$j])
                @php($s++)
                @endfor
            @endfor
        <!-- BOBOT OUTPUT -->
        @php($bzn0=0)
        @for($j=0; $j < $hl->nilai; $j++)
            @php($boo_arr[$j]=$boo_arr[$j]+$xntarget[$non-1]*0.01*$fzn_arr[$non-1+$prm][$j])
            @php($bzn0++)
            @if($bzn0==$hl->nilai)
                @php($bzn0=0)
            @endif
        @endfor
        <!-- BIAS INPUT -->
        @php($bzn0=0)
        @for($j=0; $j < $hl->nilai; $j++)
            @php($bi_arr[$j]=$bi_arr[$j]+$zn_arr[$non-1][$bzn0]*0.01)
            @php($bzn0++)
            @if($bzn0==$hl->nilai)
                @php($bzn0=0)
            @endif
        @endfor
       
             <!-- LANGKAH 1 -->
                @php($bo_arr[$not-1]=$bo_arr[$not-1]+$xntarget[$non-1]*0.01)
                @for($j=0; $j < $hl->nilai; $j++)  
                @php($sum=0)
                    @for($i=0; $i < 12; $i++)
                        @php($sum+=(($r_bobot_i[$i][$j]*$xn[$i][$non-1])))
                    @endfor
                @php($z=$bi_arr[$j]+$sum)
                @php($zn_arr[$non+$prm][]=$z)
             @endfor
             <!-- LANGKAH 2 -->
            @for($i=0; $i < $hl->nilai; $i++)
                @php($fzn=1/(1+(pow(2.71828183,-$zn_arr[$non+$prm][$i]))))
                @php($fzn_arr[$non+$prm][$i]=$fzn)
            @endfor
             <!-- LANGKAH 3 -->
            @php($yn=0)
            @for($k=0;$k<(count($boo_arr));$k++)
                @php($yn+=($fzn_arr[$non+$prm][$k]*$boo_arr[$k]))
            @endfor
            @if($non==0)
                @php($yn=$bo_arr[$non+$prm]+$yn)
            @else
                @php($yn=$bo_arr[$non-1+$prm]+$yn)
            @endif
                @php($ynn_arr[$non+$prm]=$yn)
    <!-- LANGKAH 4 -->
            @php($fyn=1/(1+(pow(2.71828183,-$ynn_arr[$non+$prm]))))
                @php($fyn_arr[$non+$prm]=$fyn)
        <tr>
            <td>Tahun Ke {{$prm+1}}</td>
            <td>
                @php($p_jan=($fyn_arr[$non+$prm]-0.1)*((max($t_x1)-min($t_x1))/0.8)+min($t_x1))
                @php($p_arr[$prm][0]=round($p_jan))
                {{round($p_jan)}}
            </td>
            <td>
                @php($p_feb=($fyn_arr[$non+$prm]-0.1)*((max($t_x2)-min($t_x2))/0.8)+min($t_x2))
                @php($p_arr[$prm][1]=round($p_feb))
                {{round($p_feb)}}
            </td>
            <td>
                @php($p_mar=($fyn_arr[$non+$prm]-0.1)*((max($t_x3)-min($t_x3))/0.8)+min($t_x3))
                @php($p_arr[$prm][2]=round($p_mar))
                {{round($p_mar)}}
            </td>
            <td>
                @php($p_apr=($fyn_arr[$non+$prm]-0.1)*((max($t_x4)-min($t_x4))/0.8)+min($t_x4))
                @php($p_arr[$prm][3]=round($p_apr))
                {{round($p_apr)}}
            </td>
            <td>
                @php($p_mei=($fyn_arr[$non+$prm]-0.1)*((max($t_x5)-min($t_x5))/0.8)+min($t_x5))
                @php($p_arr[$prm][4]=round($p_mei))
                {{round($p_mei)}}
            </td>
            <td>
                @php($p_jun=($fyn_arr[$non+$prm]-0.1)*((max($t_x6)-min($t_x6))/0.8)+min($t_x6))
                @php($p_arr[$prm][5]=round($p_jun))
                {{round($p_jun)}}
            </td>
            <td>
                @php($p_jul=($fyn_arr[$non+$prm]-0.1)*((max($t_x7)-min($t_x7))/0.8)+min($t_x7))
                @php($p_arr[$prm][6]=round($p_jul))
                {{round($p_jul)}}
            </td>
            <td>
                @php($p_agu=($fyn_arr[$non+$prm]-0.1)*((max($t_x8)-min($t_x8))/0.8)+min($t_x8))
                @php($p_arr[$prm][7]=round($p_agu))
                {{round($p_agu)}}
            </td>
            <td>
                @php($p_sep=($fyn_arr[$non+$prm]-0.1)*((max($t_x9)-min($t_x9))/0.8)+min($t_x9))
                @php($p_arr[$prm][8]=round($p_sep))
                {{round($p_sep)}}
            </td>
            <td>
                @php($p_okt=($fyn_arr[$non+$prm]-0.1)*((max($t_x10)-min($t_x10))/0.8)+min($t_x10))
                @php($p_arr[$prm][9]=round($p_okt))
                {{round($p_okt)}}
            </td>
            <td>
                @php($p_nov=($fyn_arr[$non+$prm]-0.1)*((max($t_x11)-min($t_x11))/0.8)+min($t_x11))
                @php($p_arr[$prm][10]=round($p_nov))
                {{round($p_nov)}}
            </td>
            <td>
                @php($p_des=($fyn_arr[$non+$prm]-0.1)*((max($t_x12)-min($t_x12))/0.8)+min($t_x12))
                @php($p_arr[$prm][11]=round($p_des))
                {{round($p_des)}}
            </td>
        </tr>
    @endfor
    </table>
    @endfor
</body>
</html>