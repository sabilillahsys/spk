<?php
//import Registrasi
Route::group(['middleware' => ['web','roles']],function(){
    Route::group(['roles'=>['Master','Pelanggan']],function(){
        Route::group([
            'prefix' => 'import','as' => 'import.',
        ], function(){

            Route::post('/import_penjualan/{id}','ImportController@import_penjualan')->name('import_penjualan');

        });
    });
});