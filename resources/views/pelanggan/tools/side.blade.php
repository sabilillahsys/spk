<!-- BEGIN: Main Menu-->
<div class="main-menu menu-fixed menu-light menu-accordion menu-shadow" data-scroll-to-active="true">
        <div class="main-menu-content">
        <ul class="navigation navigation-main" id="main-menu-navigation" data-menu="menu-navigation">
                <li class=" navigation-header"><span>MENU</span><i class=" feather icon-minus" data-toggle="tooltip" data-placement="right" data-original-title="General"></i>
                </li>
                <li class="nav-item {{ (request()->routeIs('pelanggan.pelanggan')) ? 'active' : '' }}"><a href="{{route('pelanggan.pelanggan')}}"><i class="feather icon-home"></i><span class="menu-title" data-i18n="Dashboard"> Dashboard</span></a>
                </li>
            </ul>
        </div>
    </div>
    <!-- END: Main Menu-->