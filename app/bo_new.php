<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class bo_new extends Model
{
    //
    protected $fillable = [
        'produk_id', 
        'nilai',
    ];
    public function produk()
    {
        return $this->belongsTo('App\produk','produk_id');
    }
}
