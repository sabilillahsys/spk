@extends('layouts.tools.layout')

@section('content')
 <!-- BEGIN: Content-->
 <div class="app-content content">
        <div class="content-overlay"></div>
        <div class="content-wrapper">
            <div class="content-header row">
            </div>
            <div class="content-body">
                <h1 class="text-primary">Sistem Peramalan</h1><strong class="text-primary">Penjualan Pada Olahan Salak</strong>
                <hr>
                <p>Selamat datang di sistem peramalan penjualan produk olahan salak pada UD Budi Jaya dengan menggunakan metode Backpropagation Network (BNN). Berikut merupakan grafik penjualan produk salak</p>
                <h2><center>Grafik Penjualan</center></h2>
                <div>
                    <canvas id="myChart"></canvas>
                </div>
            </div>
        </div>
</div>
<!-- END: Content-->

    <script src="https://cdn.jsdelivr.net/npm/chart.js"></script>
    <script>
        const labels = [
            'Jan',
            'Feb',
            'Mar',
            'Apr',
            'May',
            'Jun',
            'Jul',
            'Agu',
            'Sep',
            'Okt',
            'Nov',
            'Des',
        ];

        const data = {
            labels: labels,
            datasets: [
                @foreach($data as $datas)
                {
                label: '{{$datas->nama}}',
                backgroundColor: 'rgb({{rand(10,255)}}, {{rand(10,255)}}, {{rand(10,255)}})',
                borderColor: 'rgb({{rand(10,255)}}, {{rand(10,255)}}, {{rand(10,255)}})',
                data: [@foreach($datas->detail as $detail){{$detail->kuantitas}}, @endforeach],
                },
                @endforeach
            ]
        };

        const config = {
            type: 'line',
            data: data,
            options: {}
        };
    </script>
    <script>
        const myChart = new Chart(
            document.getElementById('myChart'),
            config
        );
    </script>
@endsection