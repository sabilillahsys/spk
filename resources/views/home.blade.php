<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta content="width=device-width, initial-scale=1.0" name="viewport">

  <title>Siti Nurizzatin Kamala</title>
    <meta name="description" content="Penerapan Sisitem Peramalan Produk Olahan Salak Menggunakan Metode BACKPROPOGATION NEURAL NETWORK">
    <meta name="keywords" content="Spk, skripsi Siti Nurizzatin Kamala, Neural Network, Backpropagation, Peramalan, Akurasi">
    <meta name="author" content="Siti Nurizzatin Kamala">

  <!-- Favicons -->
  <link href="{{asset('landing/assets/img/favicon.png')}}" rel="icon">
  <link href="{{asset('landing/assets/img/apple-touch-icon.png')}}" rel="apple-touch-icon">

  <!-- Google Fonts -->
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Roboto:300,300i,400,400i,500,500i,600,600i,700,700i|Poppins:300,300i,400,400i,500,500i,600,600i,700,700i" rel="stylesheet">

  <!-- Vendor CSS Files -->
  <link href="{{asset('landing/assets/vendor/aos/aos.css')}}" rel="stylesheet">
  <link href="{{asset('landing/assets/vendor/bootstrap/css/bootstrap.min.css')}}" rel="stylesheet">
  <link href="{{asset('landing/assets/vendor/bootstrap-icons/bootstrap-icons.css')}}" rel="stylesheet">
  <link href="{{asset('landing/assets/vendor/boxicons/css/boxicons.min.css')}}" rel="stylesheet">
  <link href="{{asset('landing/assets/vendor/glightbox/css/glightbox.min.css')}}" rel="stylesheet">
  <link href="{{asset('landing/assets/vendor/swiper/swiper-bundle.min.css')}}" rel="stylesheet">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.1.1/css/all.min.css" integrity="sha512-KfkfwYDsLkIlwQp6LFnl8zNdLGxu9YAA1QvwINks4PhcElQSvqcyVLLD9aMhXd13uQjoXtEKNosOWaZqXgel0g==" crossorigin="anonymous" referrerpolicy="no-referrer" />

  <!-- Template Main CSS File -->
  <link href="{{asset('landing/assets/css/style.css')}}" rel="stylesheet">
</head>

<body>

  <!-- ======= Top Bar ======= -->
  <section id="topbar" class="d-flex align-items-center">
    <div class="container d-flex justify-content-center justify-content-md-between">
      <div class="contact-info d-flex align-items-center">
      </div>
      <div class="social-links d-none d-md-flex align-items-center">
        <a href="#" class="twitter"><i class="bi bi-twitter"></i></a>
        <a href="#" class="facebook"><i class="bi bi-facebook"></i></a>
        <a href="#" class="instagram"><i class="bi bi-instagram"></i></a>
        <a href="#" class="linkedin"><i class="bi bi-linkedin"></i></i></a>
      </div>
    </div>
  </section>

  <!-- ======= Header ======= -->
  <header id="header" class="d-flex align-items-center">
    <div class="container d-flex align-items-center justify-content-between">

      <h1 class="logo"><a href="{{url('/')}}">BNN <span>.</span></a></h1>
      <!-- Uncomment below if you prefer to use an image logo -->
      <!-- <a href="index.html" class="logo"><img src="assets/img/logo.png" alt=""></a>-->

      <nav id="navbar" class="navbar">
        <ul>
          <li><a class="nav-link scrollto active" href="#hero">Home</a></li>
          <li><a class="nav-link scrollto" href="#about">Tentang Aplikasi</a></li>
          <li><a class="nav-link scrollto" href="#featured-services">Services</a></li>
        </ul>
        <i class="bi bi-list mobile-nav-toggle"></i>
      </nav><!-- .navbar -->

    </div>
  </header><!-- End Header -->

  <!-- ======= Hero Section ======= -->
  <section id="hero" class="d-flex align-items-center">
    <div class="container" data-aos="zoom-out" data-aos-delay="100">
      <h1>SPK - <span>  BNN</span></h1>
      <h2>Penerapan Sisitem Peramalan Produk Olahan Salak Menggunakan Metode Backpropogation Neural Network</h2>
      <div class="d-flex">
        <a href="#about" class="btn-get-started scrollto">Selengkapnya</a>
        <a href="{{url('/login')}}" class="btn-watch-video"><i class="fa fa-sign-in" aria-hidden="true"></i><span>Login Sistem</span></a>
      </div>
    </div>
  </section><!-- End Hero -->

  <main id="main">

    <!-- ======= Featured Services Section ======= -->
    <section id="featured-services" class="featured-services">
      <div class="container" data-aos="fade-up">

        <div class="row">
          <div class="col-md-6 col-lg-3 d-flex align-items-stretch mb-5 mb-lg-1">
            <div class="icon-box" data-aos="fade-up" data-aos-delay="100">
              <div class="icon"><i class="fa-solid fa-table"></i></div>
              <h4 class="title"><a href="">Data Produk</a></h4>
              <p class="description">Berfungsi untuk menyimpan data dari detail tiap produk yang terjual.</p>
            </div>
          </div>

          <div class="col-md-6 col-lg-3 d-flex align-items-stretch mb-5 mb-lg-1">
            <div class="icon-box" data-aos="fade-up" data-aos-delay="200">
              <div class="icon"><i class="fa-solid fa-table-cells"></i></div>
              <h4 class="title"><a href="">Hidden Layer</a></h4>
              <p class="description">Berfungsi Untuk Menentukan dan menyimpan data Hidden Layer</p>
            </div>
          </div>

          <div class="col-md-6 col-lg-3 d-flex align-items-stretch mb-5 mb-lg-1">
            <div class="icon-box" data-aos="fade-up" data-aos-delay="300">
              <div class="icon"><i class="fa-solid fa-table-cells-large"></i></div>
              <h4 class="title"><a href="">Bobot Input</a></h4>
              <p class="description">Berfungsi Untuk Menentukan dan menyimpan data Bobot Input setiap produk</p>
            </div>
          </div>

          <div class="col-md-6 col-lg-3 d-flex align-items-stretch mb-5 mb-lg-1">
            <div class="icon-box" data-aos="fade-up" data-aos-delay="400">
              <div class="icon"><i class="fa-solid fa-table-columns"></i></div>
              <h4 class="title"><a href="">Bobot Output</a></h4>
              <p class="description">Berfungsi Untuk Menentukan dan menyimpan data Bobot Output setiap produk</p>
            </div>
          </div>
          
          <div class="col-md-6 col-lg-3 d-flex align-items-stretch mb-5 mb-lg-1">
            <div class="icon-box" data-aos="fade-up" data-aos-delay="300">
              <div class="icon"><i class="fa-solid  fa-table-list"></i></div>
              <h4 class="title"><a href="">Bias Input</a></h4>
              <p class="description">Berfungsi Untuk Menentukan dan menyimpan data Bias Input setiap produk</p>
            </div>
          </div>

          <div class="col-md-6 col-lg-3 d-flex align-items-stretch mb-5 mb-lg-1">
            <div class="icon-box" data-aos="fade-up" data-aos-delay="400">
              <div class="icon"><i class="fa-solid fa-table-list"></i></div>
              <h4 class="title"><a href="">Bias Output</a></h4>
              <p class="description">Berfungsi Untuk Menentukan dan menyimpan data Bias Output setiap produk</p>
            </div>
          </div>
          <div class="col-md-6 col-lg-3 d-flex align-items-stretch mb-5 mb-lg-1">
            <div class="icon-box" data-aos="fade-up" data-aos-delay="400">
              <div class="icon"><i class="fa-solid  fa-table-list"></i></div>
              <h4 class="title"><a href="">Learning Rate</a></h4>
              <p class="description">Berfungsi Untuk Menentukan dan menyimpan data Learning Rate setiap produk</p>
            </div>
          </div>
          <div class="col-md-6 col-lg-3 d-flex align-items-stretch mb-5 mb-lg-1">
            <div class="icon-box" data-aos="fade-up" data-aos-delay="400">
              <div class="icon"><i class="fa-solid fa-chart-line"></i></div>
              <h4 class="title"><a href="">Prediksi</a></h4>
              <p class="description">Berfungsi Untuk menyajikan informasi hasil dari peramalan sistem dalam bentuk tabel dan chart</p>
            </div>
          </div>
        </div>

      </div>
    </section><!-- End Featured Services Section -->

    <!-- ======= About Section ======= -->
    <section id="about" class="about section-bg">
      <div class="container" data-aos="fade-up">

        <div class="section-title">
          <h2>Tentang Aplikasi</h2>
          <h3>Backpropogation <span>Neural Network</span></h3>
          <p>Aplikasi Peramalan ini menggunakan metode Backpropogation <span>Neural Network</span> dirancang berbasis website</p>
        </div>

        <div class="row">
          <div class="col-lg-6" data-aos="fade-right" data-aos-delay="100">
            <img src="assets/img/about.jpg" class="img-fluid" alt="">
          </div>
          <div class="col-lg-6 pt-4 pt-lg-0 content d-flex flex-column justify-content-center" data-aos="fade-up" data-aos-delay="100">
            <h3>Aplikasi Peramalan Penjualan</h3>
            <p class="fst-italic">
                Terdapat penelitian yang menjelaskan bahwa Peramalan pada perusahaan dapat membantu dalam merencanakan perencanaan penjualan dengan lebih baik dan meningkatkan keakurasian aktual penjualan dengan forecasting penjualan, dengan menggunakan metode pengenalan pola yaitu Neural Network dengan algoritma Backpropagation nilai akurasi yang didapatkan sangat baik
            </p>
            <ul>
            <li>
                <i class="bx bx-images"></i>
                <div>
                  <h5>Tujuan Dibuat Aplikasi</h5>
                  <p>Manfaat dari penelitian ini diharapkan yaitu dapat memberikan kemudahan UD. Budi Jaya dalam memberikan metode peramalan tepat untuk meramalkan penjualan produk yang akurat dalam melakukan peramalan terhadap prediksi penjualan produk kedepan.</p>
                </div>
              </li>
              <li>
                <i class="bx bx-store-alt"></i>
                <div>
                  <h5>Manfaat Aplikasi</h5>
                  <p>Manfaat dari penelitian ini diharapkan yaitu dapat memberikan kemudahan dalam memberikan metode peramalan tepat untuk meramalkan penjualan produk yang akurat dalam melakukan peramalan terhadap prediksi penjualan produk kedepan.</p>
                </div>
              </li>
            </ul>
            <p>
            Skenario uji coba ini dilakukan dengan tujuan untuk menguji sistem yang telah dibangun dengan cara melakukan analisis terhadap hasil, nilai MSE dan nilai MAPE, untuk skenarionya menggunakan pelatihan, pengujian.
            </p>
          </div>
        </div>

      </div>
    </section><!-- End About Section -->

  </main><!-- End #main -->

  <!-- ======= Footer ======= -->
  <footer id="footer">
    <div class="container py-4">
      <div class="copyright">
        &copy; Copyright <strong><span>Siti Nurizzatin Kamala</span></strong>. All Rights Reserved
      </div>
      <div class="credits">
        Designed by <a href="http://app.sitinurizzatinkamala.xyz//">SNK</a>
      </div>
    </div>
  </footer><!-- End Footer -->

  <div id="preloader"></div>
  <a href="#" class="back-to-top d-flex align-items-center justify-content-center"><i class="bi bi-arrow-up-short"></i></a>

  <!-- Vendor JS Files -->
  <script src="{{asset('landing/assets/vendor/purecounter/purecounter.js')}}"></script>
  <script src="{{asset('landing/assets/vendor/aos/aos.js')}}"></script>
  <script src="{{asset('landing/assets/vendor/bootstrap/js/bootstrap.bundle.min.js')}}"></script>
  <script src="{{asset('landing/assets/vendor/glightbox/js/glightbox.min.js')}}"></script>
  <script src="{{asset('landing/assets/vendor/isotope-layout/isotope.pkgd.min.js')}}"></script>
  <script src="{{asset('landing/assets/vendor/swiper/swiper-bundle.min.js')}}"></script>
  <script src="{{asset('landing/assets/vendor/waypoints/noframework.waypoints.js')}}"></script>
  <script src="{{asset('landing/assets/vendor/php-email-form/validate.js')}}"></script>

  <!-- Template Main JS File -->
  <script src="{{asset('landing/assets/js/main.js')}}"></script>

</body>

</html>