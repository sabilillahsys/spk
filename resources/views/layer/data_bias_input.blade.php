@extends('layouts.tools.layout')

@section('content')
<!-- BEGIN: Content-->
    <div class="app-content content">
        <div class="content-overlay"></div>
        <div class="content-wrapper">
            <div class="content-body">
                 <!-- Enable - disable FixedHeader table -->
                 <section id="fixedheader">
                    <div class="row">
                        <div class="col-12"> 
                            <div class="card">
                                <div class="card-header">
                                    <h4 class="card-title">Random Bias Input</h4>
                                    <a class="heading-elements-toggle"><i class="fa fa-ellipsis-v font-medium-3"></i></a>
                                    <div class="heading-elements">
                                        <ul class="list-inline mb-0">
                                            <li><a data-action="collapse"><i class="feather icon-minus"></i></a></li>
                                            <li><a data-action="reload"><i class="feather icon-rotate-cw"></i></a></li>
                                            <li><a data-action="expand"><i class="feather icon-maximize"></i></a></li>
                                            <li><a data-action="close"><i class="feather icon-x"></i></a></li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="card-content collapse show">
                                <div class="card-body card-dashboard">
                                    <div class="row" style="padding-bottom:10px;">
                                        <div class="col-md-6">
                                            <form action="{{route('layer.random_bias_input',$id)}}" method="get">
                                                @csrf
                                                <div class="row">
                                                    <div class="col col-md-7">
                                                        <div class="form-group">
                                                            <input type="number" name="h_layer" class="form-control" placeholder="Jumlah Hidden Layer" required>
                                                        </div>
                                                    </div>
                                                    <div class="col col-md-5">
                                                        <div class="form-group">       
                                                            <input type="submit" value="Proses" class="btn btn-block btn-primary">
                                                        </div>
                                                    </div>
                                                </div>
                                            </form> 
                                        </div>
                                        <div class="col-md-6">
                                            <form action="{{route('layer.input_bias_input')}}" method="post">
                                                @csrf
                                                <div class="row">
                                                    <input type="hidden" name="produk_id" class="form-control" value="{{$id}}" required>
                                                    <div class="col col-md-6">
                                                        <div class="form-group">
                                                            <input type="text" name="nilai" class="form-control" placeholder="Nilai" required>
                                                        </div>
                                                    </div>
                                                    <div class="col col-md-6">
                                                        <div class="form-group">       
                                                            <input type="submit" value="Tambah" class="btn btn-block btn-warning">
                                                        </div>
                                                    </div>
                                                </div>
                                            </form> 
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
                <!--/ Enable - disable FixedHeader table -->
                <!-- Enable - disable FixedHeader table -->
                <section id="fixedheader">
                    <div class="row">
                        <div class="col-12"> 
                            <div class="card">
                                <div class="card-header">
                                    <h4 class="card-title">Data Bias Input</h4>
                                    <a class="heading-elements-toggle"><i class="fa fa-ellipsis-v font-medium-3"></i></a>
                                    <div class="heading-elements">
                                        <ul class="list-inline mb-0">
                                            <li><a data-action="collapse"><i class="feather icon-minus"></i></a></li>
                                            <li><a data-action="reload"><i class="feather icon-rotate-cw"></i></a></li>
                                            <li><a data-action="expand"><i class="feather icon-maximize"></i></a></li>
                                            <li><a data-action="close"><i class="feather icon-x"></i></a></li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="card-content collapse show">
                                <div class="card-body card-dashboard">
                                    <div class="row" style="padding-bottom:10px;">
                                            <div class="container table-responsive">                        
                                                <table class="table table-striped table-bordered" id="data-pelanggan">
                                                    <thead>
                                                        <tr>
                                                            <th></th>
                                                            <th>No</th>
                                                            <th>Nama Produk</th>
                                                            <th>Nilai</th>
                                                        </tr>
                                                    </thead>
                                                </table>
                                            </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
                <!--/ Enable - disable FixedHeader table -->
            </div>
        </div>
    </div>
<!-- END: Content-->
@endsection
@push('scripts')
<script>
//fucktion awal
function format ( d ) {
    // `d` is the original data object for the row
    return '<table cellpadding="5" cellspacing="0" border="0" style="padding-left:50px;">'+
    '<tr>'+
            '<td>Aksi:</td>'+
            '<td>'+d.action+'</td>'+
        '</tr>'+
    '</table>';
}
//end function
$(document).ready(function() {
    var table = $('#data-pelanggan').DataTable({
        processing: true,
        serverSide: true,
        scrollCollapse: true,
        ajax: "{{route('layer.json_bias_input',$id)}}",
        columns: [
            {"className": 'details-control',"orderable": false,"data": null,"defaultContent": ''},
            { data: 'DT_RowIndex', name: 'DT_RowIndex'},
            { data: 'produk', name: 'produk' },
            { data: 'nilai', name: 'nilai' },
        ],
    });
// Add event listener for opening and closing details
    $('#data-pelanggan tbody').on('click', 'td.details-control', function () {
        var tr = $(this).closest('tr');
        var row = table.row( tr );
 
        if ( row.child.isShown() ) {
            // This row is already open - close it
            row.child.hide();
            tr.removeClass('shown');
        }
        else {
            // Open this row
            row.child( format(row.data()) ).show();
            tr.addClass('shown');
        }
    });
// Batas bawah
});
</script>
@endpush