<?php

use Illuminate\Database\Seeder;

class RoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //1
        DB::table('roles')->insert([
            'name' => "Master",
            'description' => "Digunakan sebagai Role Admin Utama",
        ]);
        DB::table('roles')->insert([
            'name' => "Pelanggan",
            'description' => "Digunakan sebagai Role Pelanggan Utama",
        ]);
        DB::table('users')->insert([
            'name'=>"Master", 
            'username'=>"master", 
            'password'=>'$2y$10$AXToTcu4tCI/EhnhHVF06e13fdEYf9JH5RcK4MJPOvPk/s3EuIjPq',
            'pic'=>"belum",
            'hp'=>"belum",
            'jk'=>"belum",
            'role_id'=>1,
        ]);
    }

}
