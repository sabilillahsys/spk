<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class data_uji extends Model
{
    protected $fillable = [
        'tahun', 
        'bulan',
        'jumlah_penjualan', 
    ];
}
