<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\data_uji;
use App\data_latih;
use App\produk;
use App\dataset;
use App\detail_penjualan_produk;
use App\hidden_layer;
use App\bobot_input;
use App\bobot_output;
use App\bias_input;
use App\bias_output;
use App\learning_rate;
use Auth;
use DataTables;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Hash;
class CetakController extends Controller
{
    public function cetak_produk($id)
    {
        $data=detail_penjualan_produk::where('produk_id','=',$id)->get();
        $tahun=detail_penjualan_produk::where('produk_id','=',$id)->select('tahun')->groupBy('tahun')->get();
        // dd($data->toArray());
        return view('cetak.produk',compact('data','tahun'));
    }
    public function cetak_produk_all()
    {
        $data=detail_penjualan_produk::all();
        $tahun=detail_penjualan_produk::selectRaw('produk_id, tahun')->groupBy('produk_id','tahun')->get();
        $produk=detail_penjualan_produk::select('produk_id')->groupBy('produk_id')->get();
        // dd($tahun->toArray());
        return view('cetak.produk_all',compact('data','tahun','produk'));
    }
    public function cetak_kom($id)
    {
        $data=detail_penjualan_produk::where('produk_id','=',$id)->get();
        $tahun=detail_penjualan_produk::where('produk_id','=',$id)->select('tahun')->groupBy('tahun')->get();
        // dd($data->toArray());
        return view('cetak.kom',compact('data','tahun'));
    }
    public function created_dataset($id)
    {
        $fi=dataset::where('produk_id',$id)->delete();
        // $fi->delete();
        $tahun=detail_penjualan_produk::where('produk_id','=',$id)->select('tahun')->groupBy('tahun')->get();
        $data_count=detail_penjualan_produk::where('produk_id','=',$id)->count();
        $data=detail_penjualan_produk::where('produk_id','=',$id)->get();
        $data=$data->toArray();
        // dd($data_count);
        for ($i=0; $i <$data_count-12; $i++) {
            // echo "<br>========= ". ($i+1)." =========<br>";
            $arr[]=null;
            $a=0;
            for ($s=$i; $s <13+$i; $s++) { 
                # code...
                $arr[$a]=$data[$s]['kuantitas'];
                // echo $data[$s]['kuantitas']." | ";
               $a++; 
            }
            // dd($arr);
            dataset::create([
                'produk_id'=> $id, 
                'x1'=> $arr[0],
                'x2'=> $arr[1],
                'x3'=> $arr[2],
                'x4'=> $arr[3],
                'x5'=> $arr[4],
                'x6'=> $arr[5],
                'x7'=> $arr[6],
                'x8'=> $arr[7],
                'x9'=> $arr[8],
                'x10'=> $arr[9],
                'x11'=> $arr[10],
                'x12'=> $arr[11],
                'target'=> $arr[12], 
        ]);
        }
        $dataset=dataset::where('produk_id',$id)->get();

        return view('cetak.kom',compact('dataset'));
    }
    public function training(Request $request,$id)
    {
        $persen=$request->get('persen');
        $batas=$request->get('batas');
        $set=dataset::where('produk_id',$id)->count();
        $limit=$set*($persen/100);
        $limit=round($limit);
        // dd($persen);
        $dataset=dataset::select('*')->where('produk_id',$id)->limit($limit)->get();
        //BIAS-BOBOT
        $hl=hidden_layer::where('produk_id',$id)->first();
        $bobot_i=bobot_input::where('produk_id',$id)->get();
        $bobot_i_arr=$bobot_i->toArray();
        $bobot_o=bobot_output::where('produk_id',$id)->get();
        $bias_i=bias_input::where('produk_id',$id)->get();
        $bias_o=bias_output::where('produk_id',$id)->first();
        $learning_rate=learning_rate::where('produk_id',$id)->first();

        return view('cetak.training',compact('dataset','bobot_i','bobot_i_arr','bobot_o','bias_i','bias_o','learning_rate','hl','batas'));
    }
    public function training_step(Request $request,$id)
    {
        $persen=$request->get('persen');
        $batas=$request->get('batas');
        $set=dataset::where('produk_id',$id)->count();
        $limit=$set*($persen/100);
        $limit=round($limit);
        // dd($persen);
        $dataset=dataset::select('*')->where('produk_id',$id)->limit($limit)->get();
        //BIAS-BOBOT
        $hl=hidden_layer::where('produk_id',$id)->first();
        // dd($hl->nilai);
        $bobot_i=bobot_input::where('produk_id',$id)->get();
        $bobot_i_arr=$bobot_i->toArray();
        $bobot_o=bobot_output::where('produk_id',$id)->get();
        $bias_i=bias_input::where('produk_id',$id)->get();
        $bias_o=bias_output::where('produk_id',$id)->first();
        $learning_rate=learning_rate::where('produk_id',$id)->first();

        return view('cetak.training_step',compact('dataset','bobot_i','bobot_i_arr','bobot_o','bias_i','bias_o','learning_rate','hl','batas'));
    }
    public function testing_step(Request $request,$id)
    {
        $produk=produk::find($id);
        $batas=$request->get('batas');
        $persen=$request->get('persen');
        $persen=100-$persen;
        $set=dataset::where('produk_id',$id)->count();
        $jum_prediksi=$request->get('jum');
        $limit=$set*($persen/100);
        $limit=round($limit);
        // dd($persen);
        $dataset=dataset::select('*')->where('produk_id',$id)->limit($limit)->get();
        $testing=dataset::select('*')->where('produk_id',$id)->skip($limit)->take($set-$limit)->get();
        //BIAS-BOBOT
        // dd($set);
        // dd($testing->toArray());
        $hl=hidden_layer::where('produk_id',$id)->first();
        $bobot_i=bobot_input::where('produk_id',$id)->get();
        $bobot_i_arr=$bobot_i->toArray();
        $bobot_o=bobot_output::where('produk_id',$id)->get();
        $bias_i=bias_input::where('produk_id',$id)->get();
        $bias_o=bias_output::where('produk_id',$id)->first();
        $learning_rate=learning_rate::where('produk_id',$id)->first();

        return view('cetak.testing_step',compact('dataset','bobot_i','bobot_i_arr','bobot_o','bias_i','bias_o','learning_rate','hl','testing','jum_prediksi','produk','batas'));
    }
    public function testing(Request $request,$id)
    {
        $produk=produk::find($id);
        $persen=$request->get('persen');
        $batas=$request->get('batas');
        $persen=100-$persen;
        $set=dataset::where('produk_id',$id)->count();
        $jum_prediksi=$request->get('jum');
        $limit=$set*($persen/100);
        $limit=round($limit);
        // dd($persen);
        $dataset=dataset::select('*')->where('produk_id',$id)->limit($limit)->get();
        $testing=dataset::select('*')->where('produk_id',$id)->skip($limit)->take($set-$limit)->get();
        //BIAS-BOBOT
        // dd($set);
        // dd($testing->toArray());
        $hl=hidden_layer::where('produk_id',$id)->first();
        $bobot_i=bobot_input::where('produk_id',$id)->get();
        $bobot_i_arr=$bobot_i->toArray();
        $bobot_o=bobot_output::where('produk_id',$id)->get();
        $bias_i=bias_input::where('produk_id',$id)->get();
        $bias_o=bias_output::where('produk_id',$id)->first();
        $learning_rate=learning_rate::where('produk_id',$id)->first();

        return view('cetak.testing',compact('dataset','bobot_i','bobot_i_arr','bobot_o','bias_i','bias_o','learning_rate','hl','testing','jum_prediksi','produk','batas'));
    }
}
