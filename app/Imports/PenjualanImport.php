<?php

namespace App\Imports;

use App\User;
use Maatwebsite\Excel\Concerns\ToModel;
use App\detail_penjualan_produk;
class PenjualanImport implements ToModel
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    protected $id;

    function __construct($id) {
    $this->id = $id;
    }
    public function model(array $row)
    {
        $cek=detail_penjualan_produk::where('bulan','=',$row['1'])->where('tahun','=',$row['2'])->where('produk_id','=',$this->id)->count();
        if ($cek==0) {
            # code...
            detail_penjualan_produk::create([
                'produk_id'=>$this->id,
                'kuantitas'=>$row['0'],
                'bulan'=>$row['1'],
                'tahun'=>$row['2'],
                'total_harga'=>$row['3'],
            ]);
        }else{
            $data=detail_penjualan_produk::where('bulan','=',$row['1'])->where('tahun','=',$row['2'])->get();
             foreach ($data as $datas) {
                 $d=detail_penjualan_produk::find($datas->id);
                 $d->delete();
             }
             detail_penjualan_produk::create([
                'produk_id'=>$this->id,
                'kuantitas'=>$row['0'],
                'bulan'=>$row['1'],
                'tahun'=>$row['2'],
                'total_harga'=>$row['3'],
            ]);
        }
                
         
    }
}
