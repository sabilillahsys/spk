<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\Hash;

class User extends Authenticatable
{
    use Notifiable;

    protected $fillable = [
        'name', 
        'username', 
        'password', 
        'jk',
        'pic',
        'hp',
        'role_id',
    ];
    public function role()
    {
        return $this->belongsTo('App\Role','role_id');
    }
    public function hasRole($roles)
    {
        $this->have_role = $this->getUserRole();
        
        if(is_array($roles)){
            foreach($roles as $need_role){
                if($this->cekUserRole($need_role)) {
                    return true;
                }
            }
        } else{
            return $this->cekUserRole($roles);
        }
        return false;
    }
    private function getUserRole()
    {
       return $this->role()->getResults();
    }
    
    private function cekUserRole($role)
    {
        return (strtolower($role)==strtolower($this->have_role->name)) ? true : false;
    }
    public function setPasswordAttribute($pass){

        $this->attributes['password'] = Hash::make($pass);
        
        }
    public function data_tes()//menampilkan user dengan data tes yang diikuti
    {
        return $this->hasMany('App\peserta_tes');
    }
}
