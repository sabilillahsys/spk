<?php
Route::group(['middleware' => ['web','roles']],function(){
    Route::group(['roles'=>['Master','Pelanggan']],function(){
        Route::group([
            'prefix' => 'pelanggan','as' => 'pelanggan.',
        ], function(){
            Route::get('/','PelangganController@pelanggan')->name('pelanggan');
            Route::get('/api','PelangganController@api')->name('api');
            //akun pelanggan
            Route::get('/data_pelanggan','PelangganController@data_pelanggan')->name('data_pelanggan');
            Route::get('/json_pelanggan','PelangganController@json_pelanggan')->name('json_pelanggan');
            Route::post('/input_pelanggan','PelangganController@input_pelanggan')->name('input_pelanggan');
            Route::post('/update_pelanggan/{id}','PelangganController@update_pelanggan')->name('update_pelanggan');
            Route::get('/delete_pelanggan/{id}','PelangganController@delete_pelanggan')->name('delete_pelanggan');
            
        });
    });
});