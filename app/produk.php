<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class produk extends Model
{
    protected $fillable = [
        'nama', 
        'harga',
        'jenis', 
        'jumlah', 
    ];
    public function detail()//menampilkan user dengan data tes yang diikuti
    {
        return $this->hasMany('App\detail_penjualan_produk');
    }
}
