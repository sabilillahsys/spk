<?php
Route::group(['middleware' => ['web','roles']],function(){
    Route::group(['roles'=>['Master']],function(){
        Route::group([
            'prefix' => 'uji','as' => 'uji.',
        ], function(){
            Route::get('/api','DataController@api')->name('api');
            Route::get('/data_uji','DataController@data_uji')->name('data_uji');
            Route::get('/json_uji','DataController@json_uji')->name('json_uji');
            Route::post('/input_uji','DataController@input_uji')->name('input_uji');
            Route::post('/update_uji/{id}','DataController@update_uji')->name('update_uji');
            Route::get('/delete_uji/{id}','DataController@delete_uji')->name('delete_uji');
            
        });
    });
});

Route::group(['middleware' => ['web','roles']],function(){
    Route::group(['roles'=>['Master']],function(){
        Route::group([
            'prefix' => 'latih','as' => 'latih.',
        ], function(){
            Route::get('/api','DataController@api')->name('api');
            Route::get('/data_latih','DataController@data_latih')->name('data_latih');
            Route::get('/json_latih','DataController@json_latih')->name('json_latih');
            Route::post('/input_latih','DataController@input_latih')->name('input_latih');
            Route::post('/update_latih/{id}','DataController@update_latih')->name('update_latih');
            Route::get('/delete_latih/{id}','DataController@delete_latih')->name('delete_latih');
            
        });
    });
});