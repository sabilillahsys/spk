<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class dataset extends Model
{
    //
    protected $fillable = [
        'produk_id', 
        'x1',
        'x2',
        'x3',
        'x4',
        'x5',
        'x6',
        'x7',
        'x8',
        'x9',
        'x10',
        'x11',
        'x12',
        'target',
    ];
    public function produk()
    {
        return $this->belongsTo('App\produk','produk_id');
    }
}
